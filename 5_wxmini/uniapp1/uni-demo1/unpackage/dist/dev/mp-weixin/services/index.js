"use strict";
const common_vendor = require("../common/vendor.js");
const request = (url, options = {}) => {
  return new Promise((resolve, reject) => {
    common_vendor.index.request({
      url,
      ...options,
      success: resolve,
      fail: reject
    });
  });
};
const getGoodsApi = () => {
  return request("http://zyxcl.xyz/exam_api/goods");
};
exports.getGoodsApi = getGoodsApi;
