"use strict";
const common_vendor = require("../../common/vendor.js");
if (!Array) {
  const _easycom_uni_countdown2 = common_vendor.resolveComponent("uni-countdown");
  const _easycom_uni_list_chat2 = common_vendor.resolveComponent("uni-list-chat");
  const _easycom_uni_icons2 = common_vendor.resolveComponent("uni-icons");
  const _easycom_uni_list2 = common_vendor.resolveComponent("uni-list");
  (_easycom_uni_countdown2 + _easycom_uni_list_chat2 + _easycom_uni_icons2 + _easycom_uni_list2)();
}
const _easycom_uni_countdown = () => "../../uni_modules/uni-countdown/components/uni-countdown/uni-countdown.js";
const _easycom_uni_list_chat = () => "../../uni_modules/uni-list/components/uni-list-chat/uni-list-chat.js";
const _easycom_uni_icons = () => "../../uni_modules/uni-icons/components/uni-icons/uni-icons.js";
const _easycom_uni_list = () => "../../uni_modules/uni-list/components/uni-list/uni-list.js";
if (!Math) {
  (_easycom_uni_countdown + _easycom_uni_list_chat + _easycom_uni_icons + _easycom_uni_list)();
}
const _sfc_main = {
  __name: "mine",
  setup(__props) {
    console.log(common_vendor.md5Exports("1234567890"));
    return (_ctx, _cache) => {
      return {
        a: common_vendor.p({
          day: 1,
          hour: 1,
          minute: 12,
          second: 40
        }),
        b: common_vendor.p({
          ["avatar-circle"]: true,
          title: "uni-app",
          avatar: "https://qiniu-web-assets.dcloud.net.cn/unidoc/zh/unicloudlogo.png",
          note: "您收到一条新的消息",
          time: "2020-02-02 20:20"
        }),
        c: common_vendor.p({
          title: "uni-app",
          avatar: "https://qiniu-web-assets.dcloud.net.cn/unidoc/zh/unicloudlogo.png",
          note: "您收到一条新的消息",
          time: "2020-02-02 20:20",
          ["badge-text"]: "12"
        }),
        d: common_vendor.p({
          title: "uni-app",
          avatar: "https://qiniu-web-assets.dcloud.net.cn/unidoc/zh/unicloudlogo.png",
          note: "您收到一条新的消息",
          time: "2020-02-02 20:20",
          ["badge-positon"]: "left",
          ["badge-text"]: "dot"
        }),
        e: common_vendor.p({
          title: "uni-app",
          avatar: "https://qiniu-web-assets.dcloud.net.cn/unidoc/zh/unicloudlogo.png",
          note: "您收到一条新的消息",
          time: "2020-02-02 20:20",
          ["badge-positon"]: "left",
          ["badge-text"]: "99"
        }),
        f: common_vendor.p({
          title: "uni-app",
          ["avatar-list"]: _ctx.avatarList,
          note: "您收到一条新的消息",
          time: "2020-02-02 20:20",
          ["badge-positon"]: "left",
          ["badge-text"]: "dot"
        }),
        g: common_vendor.p({
          type: "star-filled",
          color: "#999",
          size: "18"
        }),
        h: common_vendor.p({
          title: "uni-app",
          ["avatar-list"]: _ctx.avatarList,
          note: "您收到一条新的消息",
          time: "2020-02-02 20:20",
          ["badge-positon"]: "left",
          ["badge-text"]: "dot"
        }),
        i: common_vendor.p({
          border: true
        })
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "/Users/zhaoyaxiang/Desktop/02/5_wxmini/uniapp1/uni-demo1/pages/mine/mine.vue"]]);
wx.createPage(MiniProgramPage);
