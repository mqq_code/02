"use strict";
const common_vendor = require("../../common/vendor.js");
const services_index = require("../../services/index.js");
const _sfc_main = {
  __name: "index",
  setup(__props) {
    const goods = common_vendor.ref([]);
    const curIndex = common_vendor.ref(0);
    const groupRef = common_vendor.ref([]);
    const scrollTop = common_vendor.ref(0);
    const offsetTopList = common_vendor.ref([]);
    const cartList = common_vendor.ref([]);
    const changeIndex = (index) => {
      curIndex.value = index;
      scrollTop.value = groupRef.value[index].$el.offsetTop;
    };
    const scrollList = (e) => {
      const index = offsetTopList.value.findIndex((v, i) => {
        if (i + 1 === offsetTopList.value.length)
          return true;
        return e.detail.scrollTop >= v && e.detail.scrollTop < offsetTopList.value[i + 1];
      });
      curIndex.value = index;
    };
    const getGoods = async () => {
      const res = await services_index.getGoodsApi();
      goods.value = res.data.data;
      common_vendor.nextTick$1(() => {
        offsetTopList.value = groupRef.value.map((v) => v.$el.offsetTop);
        console.log(offsetTopList.value);
      });
    };
    getGoods();
    const changeCount = (food, num) => {
      if (typeof food.count === "number") {
        food.count += num;
      } else {
        food.count = 1;
      }
      if (!cartList.value.find((v) => v === food)) {
        cartList.value.push(food);
      } else {
        if (food.count === 0) {
          const index = cartList.value.indexOf(food);
          cartList.value.splice(index, 1);
        }
      }
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.f(goods.value, (item, index, i0) => {
          return {
            a: common_vendor.n(`icon${item.type}`),
            b: common_vendor.t(item.name),
            c: item.name,
            d: common_vendor.n({
              active: curIndex.value === index
            }),
            e: common_vendor.o(($event) => changeIndex(index), item.name)
          };
        }),
        b: common_vendor.f(goods.value, (item, k0, i0) => {
          return {
            a: common_vendor.t(item.name),
            b: common_vendor.f(item.foods, (food, k1, i1) => {
              return common_vendor.e({
                a: food.image,
                b: common_vendor.t(food.name),
                c: common_vendor.t(food.price),
                d: food.count > 0
              }, food.count > 0 ? {
                e: common_vendor.o(($event) => changeCount(food, -1), food.name),
                f: common_vendor.t(food.count)
              } : {}, {
                g: common_vendor.o(($event) => changeCount(food, 1), food.name),
                h: food.name
              });
            }),
            c: item.name
          };
        }),
        c: scrollTop.value,
        d: common_vendor.o(scrollList),
        e: common_vendor.f(cartList.value, (item, k0, i0) => {
          return {
            a: common_vendor.t(item.name),
            b: common_vendor.t(item.count),
            c: item.name
          };
        })
      };
    };
  }
};
const MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__scopeId", "data-v-1cf27b2a"], ["__file", "/Users/zhaoyaxiang/Desktop/02/5_wxmini/uniapp1/uni-demo1/pages/index/index.vue"]]);
wx.createPage(MiniProgramPage);
