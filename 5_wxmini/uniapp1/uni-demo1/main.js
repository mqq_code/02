import App from './App'
import * as Pinia from 'pinia';


// #ifdef H5 || MP-WEIXIN
console.log('这是h5页面11111111')
// #endif


import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  app.use(Pinia.createPinia())
  
  return {
    app,
    Pinia
  }
}