

  
  export const request = (url, options = {}) => {
    return new Promise((resolve, reject) => {
      uni.request({
        url,
        ...options,
        success: resolve,
        fail: reject
      })
    })
  }
  
  export const getGoodsApi = () => {
    // /api/sell/api/goods
    return request('http://zyxcl.xyz/exam_api/goods')
  }
  