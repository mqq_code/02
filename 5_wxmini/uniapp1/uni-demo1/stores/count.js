import { defineStore } from 'pinia';
import { ref } from 'vue'

export const useCounterStore = defineStore('counter', () => {
  const c = ref(10)
  const title = ref('标题')
  
  
  return {
    c,
    title
  }
});