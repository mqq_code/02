// pages/mine/mine.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: '标题',
    form: {
      name: '小明'
    },
    list: [1,2,3,4,5,6,7,8,9,10],
    banners: []
  },

  scroll() {
    console.log('list滚动了');
  },

  changeName(e) {
    console.log(e.detail.value);
    this.setData({
      'form.name': e.detail.value
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    wx.request({
      url: 'http://zyxcl.xyz/music_api/banner',
      success: (res) => {
        console.log(res.data.banners);
        this.setData({
          banners: res.data.banners
        })
      }
    })
    console.log('%c onload', 'font-size: 20px;color: red;');
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    console.log('%c onReady', 'font-size: 20px;color: red;');

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    console.log('%c onShow', 'font-size: 20px;color: red;');

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {
    console.log('%c onHide', 'font-size: 20px;color: green;');
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    console.log('%c onUnload', 'font-size: 20px;color: green;');
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})