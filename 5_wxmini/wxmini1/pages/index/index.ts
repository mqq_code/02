// index.js
import moment from 'moment'

Page({
  data: {
    date: '',
    show: false,
    list: [
      { name: '小明1', age: 20, id: 1 },
      { name: '小明2', age: 25, id: 2 },
      { name: '小明3', age: 27, id: 3 },
      { name: '小明4', age: 30, id: 4 },
      { name: '小明5', age: 33, id: 5 }
    ]
  },
  onDisplay() {
    this.setData({ show: true });
  },
  onClose() {
    this.setData({ show: false });
  },
  formatDate(date) {
    date = new Date(date);
    return `${date.getMonth() + 1}/${date.getDate()}`;
  },
  onConfirm(event) {
    this.setData({
      show: false,
      date: this.formatDate(event.detail),
    });
  },
  goDetail(e) {
    const { id, name, age } = e.currentTarget.dataset.item
    wx.navigateTo({
      url: `/pages/detail/detail?id=${id}&name=${name}&age=${age}`,
    })
  },
  add() {},
  onLoad() {
    wx.showToast({
      title: '成功',
      icon: 'success',
      duration: 2000
    })
    wx.showShareMenu({
      withShareTicket: true,
      menus: ['shareAppMessage', 'shaeTimeline']
    })
    const t = Date.now()

    
    console.log(moment(t).format('YYYY-MM-DD kk:mm:ss'));


  },
  onReachBottom() {
    console.log('到最底部了，调用下一页接口');
  },

  onShareAppMessage() {
    return {
      title: '砍一刀',
      path: '/pages/search/search',
      imageUrl: '/assets/home.png'
    }
  }
})
