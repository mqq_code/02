// pages/cinema/cinema.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: '标题',
    count: 0,
    arr: [1,2,3,4,5,6],
    obj: {
      name: '小明',
      age: 20
    }
  },

  clickfn() {
    // 更新页面
    this.setData({
      count: this.data.count + 10,
      'obj.age': this.data.obj.age + 1
    })
    console.log('点击了按钮', this.data.count);
  },

  sub(e) {
    // 获取标签上的自定义属性
    const { num } = e.currentTarget.dataset
    this.setData({
      count: this.data.count + num
    })
    console.log(e.currentTarget.dataset);
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})