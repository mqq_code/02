// pages/game/game.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    diceArr: [1,2,3],
    chipMoney: 0,
    userMoney: 10000
  },

  add(e) {
    // 接收子组件点击的筹码
    this.setData({
      chipMoney: this.data.chipMoney + e.detail
    })
  },

  clear() {
    this.setData({
      chipMoney: 0
    })
  },

  async start(e) {
    if (this.data.chipMoney === 0) {
      wx.showToast({
        title: '请下注',
        icon: 'error'
      })
      return
    }
    console.log('接收子组件数据', e.detail);
    const type = e.detail
    const result = await this.run()
    if (this.getResult(result) === type) {
      this.setData({
        userMoney: this.data.userMoney + this.data.chipMoney
      })
      wx.showToast({
        title: '赢了',
      })
    } else {
      this.setData({
        userMoney: this.data.userMoney - this.data.chipMoney
      })
      wx.showToast({
        title: '输了',
      })
    }
  },

  getResult(arr) {
    if (arr[0] === arr[1] && arr[0] === arr[2]) {
      return '2'
    }
    const total = arr.reduce((prev, val) => prev + val)
    if (total > 10) {
      return '1'
    }
    return '3'
  },

  run() {
    return new Promise((resolve) => {
      let t = 0
      const timer = setInterval(() => {
        this.setData({
          diceArr: this.data.diceArr.map(this.random)
        })
        t += 100
        if (t >= 3000) {
          clearInterval(timer)
          resolve(this.data.diceArr)
        }
      }, 100)
    })
  },

  random() {
    return Math.floor(Math.random() * 6) + 1
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})