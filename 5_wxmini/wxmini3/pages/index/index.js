// index.js
Page({
  data: {
    count: 0,
    title: '标题'
  },
  changeCount(e) {
    const { num } = e.target.dataset
    this.setData({
      count: this.data.count + num
    })
  },
  change(e) {
    console.log('子组件调用此函数，接收子组件传入的参数', e.detail);
    this.setData({
      count: this.data.count + e.detail
    })
  }
})
