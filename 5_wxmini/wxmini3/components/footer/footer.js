// components/footer/footer.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    start(e) {
      const { type } = e.target.dataset
      // 调用父组件方法，给父组件传入点击的按钮类型
      this.triggerEvent('click', type)
    }
  }
})