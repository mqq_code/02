// components/button-counter/button-counter.js
Component({
  options: {
    multipleSlots: true // 在组件定义时的选项中启用多slot支持
  },
  /**
   * 组件的属性列表
   */
  properties: {
    title: String,
    count: Number
  },

  /**
   * 组件的初始数据
   */
  data: {
    num: 0
  },

  /**
   * 组件的方法列表
   */
  methods: {
    clickBtn() {
      console.log('点击子组件按钮');
      // 触发父组件传入的事件
      this.triggerEvent('change123', 10)
    }
  },
  // 监听变量，类似vue中的watch
  observers: {
    count(newVal) {
      console.log('count改变了', newVal);
    }
  },
  // 组件的生命周期
  lifetimes: {
    attached: function() {
      // 在组件实例进入页面节点树时执行, 
      console.log('%c 加载组件', 'color: green;font-size: 20px');
      this.timer = setInterval(() => {
        this.setData({
          num: this.data.num + 1
        })
        console.log(this.data.num);
      }, 1000)
    },
    detached: function() {
      // 在组件实例被从页面节点树移除时执行
      console.log('%c 销毁组件', 'color: red;font-size: 20px');
      clearInterval(this.timer)
    },
  },
  // 组件所在页面的生命周期
  pageLifetimes: {
    hide() {
      console.log('%c 页面看不见了', 'color: green;font-size: 20px');
      clearInterval(this.timer)
    },
    show() {
      this.timer = setInterval(() => {
        this.setData({
          num: this.data.num + 1
        })
        console.log(this.data.num);
      }, 1000)
    }
  }
})