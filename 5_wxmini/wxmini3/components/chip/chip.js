// components/chip/chip.js
Component({

  /**
   * 组件的属性列表
   */
  properties: {
    chipMoney: Number
  },

  /**
   * 组件的初始数据
   */
  data: {
    chipList: [50, 100, 200, 500]
  },

  /**
   * 组件的方法列表
   */
  methods: {
    clear() {
      this.triggerEvent('clear')
    },
    add(e) {
      const { num } = e.target.dataset
      console.log('num传给父组件', num);
      this.triggerEvent('add', num)
    },
  }
})