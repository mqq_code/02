import React from 'react'

interface Props {
  count: number;
  name: string;
  onAdd: (n: number) => void;
}

const Child: React.FC<Props> = (props) => {
  return (
    <div className='box'>
      <h2>Child</h2>
      <p>count: {props.count}</p>
      <button onClick={() => props.onAdd(1)}>count + 1</button>
    </div>
  )
}

export default Child