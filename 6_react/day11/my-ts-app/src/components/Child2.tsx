import React, { Component } from 'react'

interface Props {
  count: number;
  name: string;
  onAdd: (n: number) => void;
}
interface State {
  title: string;
}

class Child2 extends Component<Props, State> {

  state = {
    title: '默认标题'
  }


  render() {
    console.log(this.props.count)
    return (
      <div className='box'>
        <h2>Child2 - {this.state.title}</h2>
      </div>
    )
  }
}

export default Child2