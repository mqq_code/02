import React, { useEffect, useRef, useState } from 'react'
import axios from 'axios'
import Child from './components/Child'
import Child2 from './components/Child2'

interface BannerItem {
  imageUrl: string;
  targetId: number;
}
interface Form {
  name: string;
  age: number;
}

const App: React.FC = () => {
  const h1Ref = useRef<HTMLHeadingElement>(null)
  const [count, setCount] = useState<number>(0)
  const [banners, setBanners] = useState<BannerItem[]>([])

  const [form, setForm] = useState<Form>({
    name: ''
  } as Form)

  useEffect(() => {
    console.log(h1Ref.current?.outerHTML)
    axios.get<{ banners: BannerItem[] }>('https://zyxcl.xyz/music/api/banner')
      .then(res => {
        setBanners(res.data.banners)
      })
  }, [])

  const changeName = (e: React.ChangeEvent<HTMLInputElement>) => {
    setForm({
      ...form,
      name: e.target.value
    })
  }
  const clickh1 = (e: React.MouseEvent<HTMLHeadingElement, MouseEvent>) => {
    console.log((e.target as HTMLHeadingElement).innerHTML)
  }


  return (
    <div>
      <h1 onClick={clickh1} ref={h1Ref}>App</h1>
      <p>count: {count}</p>
      <button onClick={() => setCount(count + 1)}>+</button>
      <div>姓名: {form.name}</div>
      <input type="text" value={form.name} onChange={changeName} />
      {/* <ul>
        {banners.map(it => 
          <li key={it.targetId}>
            <img src={it.imageUrl} width={300} alt="" />
          </li>
        )}
      </ul> */}
      <Child count={count} name={form.name} onAdd={n => setCount(count + n)} />
      <Child2 count={count} name={form.name} onAdd={n => setCount(count + n)} />
    </div>
  )
}

export default App