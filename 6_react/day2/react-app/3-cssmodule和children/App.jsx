import React, { Component } from 'react'
import Child from './components/Child'

class App extends Component {

  box = () => {
    return (
      <ul>
        <li>11111</li>
        <li>22222</li>
      </ul>
    )
  }

  render() {
    
    return (
      <div>
        <h1 className='title'>App</h1>
        <div className="box">app</div>
        <Child
          second={<div>222222222</div>}
          third={this.box()}
        >
          <p>111111</p>
          <p>22222</p>
          <p>33333</p>
        </Child>
      </div>
    )
  }
}
export default App