import React, { Component } from 'react'
import classNames from 'classnames'
import style from './child.module.css'
// cssmodule: 样式隔离

class Child extends Component {
  state = {
    curIndex: 1
  }
  change = () => {
    this.setState({
      curIndex: this.state.curIndex === 1 ? 0 : 1
    })
  }
  render() {
    return (
      <div className={style.childWrap}>
        <h2>Child</h2>
        <button onClick={this.change}>按钮</button>
        {/* <div className={`${style.box} bold`}>child</div> */}
        <div className={classNames('bold', style.box, { [style.active]: this.state.curIndex === 0 })}>child</div>
        <div className={style.row}>{this.props.children}</div>
        <div className={style.row}>{this.props.second}</div>
        <div className={style.row}>{this.props.third}</div>
      </div>
    )
  }
}

export default Child