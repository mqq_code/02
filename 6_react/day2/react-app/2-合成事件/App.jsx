import React, { Component } from 'react'

class App extends Component {

  state = {
    count: 0
  }

  add1 = (e) => {
    // e: 合成事件对象，经过react包装之后的事件对象
    // e.stopPropagation()
    console.log(e)
    console.log('原本的事件对象', e.nativeEvent)
  }

  add2() {
    console.log(this)
  }

  componentDidMount() {
    document.querySelector('h2').addEventListener('click', e => {
      console.log(e)
    })
  }

  render() {
    console.log(this)
    return (
      <div>
        <h2>this指向</h2>
        {this.state.count}
        <button onClick={this.add1}>按钮1</button>
        {/* 合成事件：利用事件冒泡的原理把所有事件都绑定到了 document 上，然后根据事件触发的元素去查找要执行的函数 */}
        {/* 
          合成事件:
            1. React 自己实现了一套事件注册、分发的机制，统一了不同浏览器之间事件系统的差异, 例如阻止冒泡统一使用 e.stopPropagation()。
            2. React 组件上声明的事件最终绑定到了 document 这个 DOM 节点上，而不是 React 组件对应的 DOM 节点。只有document这个节点上面才绑定了DOM原生事件，其他节点没有绑定事件。这样简化了DOM原生事件，减少了内存开销。
        */}
        <button onClick={this.add2.bind(this)}>按钮2</button>
        <button onClick={() => this.add2()}>按钮3</button>
      </div>
    )
  }
}

export default App

// const list = {
//   onClick: []
// }

// list.onClick.push({
//   el: button,
//   event: fn
// })

// const fn = list.onClick.find(it => it.el === e.target).fn

// fn()



