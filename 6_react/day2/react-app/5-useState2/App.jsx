import React, { useState } from 'react'


// 函数组件在 react v16.8 之前不能定义状态
// 16.8 新增了hooks，解决函数组件的状态问题，可以完全代替类组件的功能
// hook: 名字以 use 开头的函数
// hook使用规则：
//   1. 只能在函数组件的最顶层使用，不能在 if、for、子函数中使用
//   2. 只能在函数组件和自定义hook中使用，不能在class组件和普通函数中使用


const App = () => {
  const [count, setCount] = useState(10)


  const add = () => {
    setCount(prev => {
      console.log(prev)
      return prev + 1
    })
    setCount(prev => {
      console.log(prev)
      return prev + 1
    })
    setCount(prev => {
      console.log(prev)
      return prev + 1
    })
  }

  console.log('渲染组件')

  return (
    <div>
      {count}
      <button onClick={add}>+</button>
    </div>
  )
}

export default App