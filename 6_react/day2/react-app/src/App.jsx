import React, { useState, useEffect, useRef } from 'react'

const App = () => {
  const [num, setNum] = useState(0)
  const domRef = useRef()
  const inpRef = useRef()

  useEffect(() => {
    console.log(domRef.current)
  }, [num])

  return (
    <div>
      <input type="text" ref={inpRef} />
      <p ref={domRef}>{num}</p>
      <button onClick={() => setNum(num + 1)}>+</button>
      <button onClick={() => {
        console.log(inpRef.current.value)
      }}>获取inp</button>
    </div>
  )
}

export default App

