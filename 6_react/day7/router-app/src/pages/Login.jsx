import React from 'react'
import { useSearchParams, useNavigate } from 'react-router-dom'

const Login = () => {
  const [searchParams] = useSearchParams()
  const navigate = useNavigate()
  return (
    <div>
      <h1>Login</h1>
      <button onClick={() => {
        localStorage.setItem('token', 'token123')
        const redirectUrl = searchParams.get('redirectUrl') || '/'
        navigate(redirectUrl)
      }}>登录</button>
    </div>
  )
}

export default Login