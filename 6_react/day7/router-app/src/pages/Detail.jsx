import React, { useEffect, useState } from 'react'
import { useLocation, useSearchParams, useParams } from 'react-router-dom'

const Detail = () => {
  const [info, setInfo] = useState({})
  const location = useLocation() // 获取当前路由信息
  const [searchParams, setSearchParams] = useSearchParams() // 获取search参数
  const params = useParams() // 获取动态路由参数

  console.log(location)
  // console.log(searchParams.getAll('a'))
  // console.log(searchParams.get('id'))
  console.log(params)
  
  useEffect(() => {
    // const id = searchParams.get('id')
    const id = params.id
    fetch(`https://zyxcl.xyz/music/api/playlist/detail?id=${id}`)
      .then(res => res.json())
      .then(res => {
        setInfo(res.playlist)
      })

  }, [location])

  return (
    <div>
      <button onClick={() => {
        setSearchParams('id=6655442283')
      }}>修改id</button>
      <h2>{info.name}</h2>
      <p>更新时间： {new Date(info.updateTime).toLocaleString()}</p>
      <img src={info.coverImgUrl} width={100} alt="" />
      <p>{info.description}</p>
    </div>
  )
}

export default Detail