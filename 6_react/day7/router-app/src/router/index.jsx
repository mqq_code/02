import { Navigate } from 'react-router-dom'
import { lazy, Suspense } from 'react'
import Auth from '../hoc/Auth'
import Home from '../pages/Home'
import Detail from '../pages/Detail'
import About from '../pages/About'
import Child1 from '../pages/Child1'
import Login from '../pages/Login'
// import Child2 from '../pages/Child2'
import Child3 from '../pages/Child3'

// 异步组件\组件懒加载：必须配合 Suspense 组件使用，lazy引入的组件必须是 Suspense 的后代组件
const Child2 = lazy(() => import('../pages/Child2'))
// const Child3 = lazy(() => import('../pages/Child3'))

const routes = [
  {
    path: '/',
    element: <Navigate to="/home" />
  },
  {
    path: '/home',
    element: <Home />,
    isAuth: true,
    title: '首页',
    children: [
      {
        path: '/home',
        element: <Navigate to="/home/child1" />
      },
      {
        path: '/home/child1',
        element: <Child1 />,
        title: 'child1',
      },
      {
        path: '/home/child2',
        element: <Child2 />,
        title: 'child2',
      },
      {
        path: '/home/child3',
        element: <Child3 />,
        title: 'child3',
      }
    ]
  },
  {
    path: '/detail/:id',
    element: <Detail />,
    title: '详情',
    isAuth: true
  },
  {
    path: '/about',
    element: <About />,
    title: '关于我们',
  },
  {
    path: '/login',
    element: <Login />,
    title: '登录',
  },
  {
    path: '*',
    element: <div>404</div>
  }
]

const RouteRender = (props) => {
  document.title = props.title
  return props.children
}


const formatRoutes = (routes) => {
  if (!Array.isArray(routes)) return []
  
  return routes.map(item => {
    return {
      ...item,
      element: (
        <RouteRender title={item.title}>
          <Auth isAuth={item.isAuth}>
            {item.element}
          </Auth>
        </RouteRender>
      ),
      children: formatRoutes(item.children)
    }
  })
}



export default formatRoutes(routes)