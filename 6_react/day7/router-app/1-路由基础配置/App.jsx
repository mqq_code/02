import React from 'react'
import {
  Routes, // 路由的外层组件
  Route, // 配置路由组件
  Navigate, // 重定向组件
  Link, // 跳转，没有高亮类名
  NavLink // 跳转，自动添加高亮类名
} from 'react-router-dom'
import Home from './pages/Home'
import About from './pages/About'

const App = () => {
  return (
    <div>
      <nav>
        <NavLink to="/home">首页</NavLink>
        <NavLink to="/about">关于我们</NavLink>
      </nav>
      <Routes>
        <Route path="/" element={<Navigate to="/home" />} />
        <Route path="/home" element={<Home />} />
        <Route path="/about" element={<About />} />
        <Route path="*" element={<div>404</div>} />
      </Routes>
    </div>
  )
}

export default App