import React from 'react'
import {
  useRoutes, // 配置路由的hook
  NavLink // 跳转，自动添加高亮类名
} from 'react-router-dom'
import routeConfig from './router/index'

const App = () => {
  const routes = useRoutes(routeConfig)

  return (
    <div>
      <nav>
        <NavLink to="/home">首页</NavLink>
        <NavLink to="/about">关于我们</NavLink>
      </nav>
      {routes}
    </div>
  )
}

export default App