import React from 'react'
import { Outlet, NavLink } from 'react-router-dom'
const Home = () => {
  return (
    <div>
      <div className="header">
        <NavLink to="/home/child1">导航1</NavLink>
        <NavLink to="/home/child2">导航2</NavLink>
        <NavLink to="/home/child3">导航3</NavLink>
      </div>
      <div className="content">
        <Outlet />
      </div>
    </div>
  )
}

export default Home