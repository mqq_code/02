import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'

const Child1 = () => {
  const [playlist, setPlaylist] = useState([])
  const navigate = useNavigate()

  useEffect(() => {
    fetch('https://zyxcl.xyz/music/api/top/playlist/highquality')
      .then(res => res.json())
      .then(res => {
        console.log(res.playlists)
        setPlaylist(res.playlists)
      })
  }, [])

  const goDetail = id => {
    // navigate(`/detail?id=${id}&a=100&b=200&a=300`)
    // navigate(`/detail/${id}`)
    navigate(`/detail/${id}`, {
      state: {
        a: 100,
        b: 200,
        c: [1,2,3,4,5]
      }
    })
  }

  return (
    <div className='child'>
      {playlist.map(item =>
        <div key={item.id} onClick={() => goDetail(item.id)}>
          <h3>{item.name}</h3>
          <img src={item.coverImgUrl} width={100} alt="" />
          <p>{item.description}</p>
        </div>
      )}
    </div>
  )
}

export default Child1