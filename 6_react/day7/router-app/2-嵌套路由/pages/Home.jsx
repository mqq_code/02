import React from 'react'
import { Outlet } from 'react-router-dom'
const Home = () => {
  return (
    <div>
      <div className="header">home 头部区域</div>
      <div className="content">
        <Outlet />
      </div>
    </div>
  )
}

export default Home