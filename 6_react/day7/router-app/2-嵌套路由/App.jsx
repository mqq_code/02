import React from 'react'
import {
  Routes, // 路由的外层组件
  Route, // 配置路由组件
  Navigate, // 重定向组件
  Link, // 跳转，没有高亮类名
  NavLink // 跳转，自动添加高亮类名
} from 'react-router-dom'
import Home from './pages/Home'
import About from './pages/About'
import Child1 from './pages/Child1'
import Child2 from './pages/Child2'
import Child3 from './pages/Child3'

const App = () => {
  return (
    <div>
      <nav>
        <NavLink to="/home">首页</NavLink>
        <NavLink to="/about">关于我们</NavLink>
      </nav>
      <Routes>
        <Route path="/" element={<Navigate to="/home" />} />
        <Route path="/home" element={<Home />}>
          <Route path="/home" element={<Navigate to="/home/child1" />} />
          <Route path="/home/child1" element={<Child1 />} />
          <Route path="/home/child2" element={<Child2 />} />
          <Route path="/home/child3" element={<Child3 />} />
        </Route>
        <Route path="/about" element={<About />} />
        <Route path="*" element={<div>404</div>} />
      </Routes>
    </div>
  )
}

export default App