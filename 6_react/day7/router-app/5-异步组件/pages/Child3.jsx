import React, { useState, Suspense } from 'react'
const Box = React.lazy(() => import('../components/Box'))

const Child3 = () => {
  const [show, setShow] = useState(false)
  return (
    <div>
      <h2>Child3</h2>
      <button onClick={() => setShow(true)}>展示</button>
      <Suspense fallback={<div style={{ width: 300, height: 300, background: 'blue' }}>loading</div>}>
        {show &&  <Box />}
      </Suspense>
    </div>
  )
}

export default Child3