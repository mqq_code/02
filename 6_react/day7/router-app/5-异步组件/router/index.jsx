import { Navigate } from 'react-router-dom'
import { lazy, Suspense } from 'react'
import Home from '../pages/Home'
import Detail from '../pages/Detail'
import About from '../pages/About'
import Child1 from '../pages/Child1'
// import Child2 from '../pages/Child2'
import Child3 from '../pages/Child3'

// 异步组件\组件懒加载：必须配合 Suspense 组件使用，lazy引入的组件必须是 Suspense 的后代组件
const Child2 = lazy(() => import('../pages/Child2'))
// const Child3 = lazy(() => import('../pages/Child3'))

export default [
  {
    path: '/',
    element: <Navigate to="/home" />
  },
  {
    path: '/home',
    element: <Home />,
    children: [
      {
        path: '/home',
        element: <Navigate to="/home/child1" />
      },
      {
        path: '/home/child1',
        element: <Child1 />
      },
      {
        path: '/home/child2',
        element: <Child2 />
      },
      {
        path: '/home/child3',
        element: <Child3 />
      }
    ]
  },
  {
    path: '/detail/:id',
    element: <Detail />
  },
  {
    path: '/about',
    element: <About />
  },
  {
    path: '*',
    element: <div>404</div>
  }
]