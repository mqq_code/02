import React, { useState } from 'react'
import style from './Login.module.scss'
import Loginform from './components/loginForm/Loginform'
import RegisterForm from './components/registerForm/RegisterForm'

const Login: React.FC = () => {
  const [isLogin, setIsLogin] = useState(true)
  const toggle = () => setIsLogin(!isLogin)
  return (
    <div className={style.page}>
      {isLogin ?
        <Loginform toggle={toggle} />
      :
        <RegisterForm toggle={toggle} />
      }
    </div>
  )
}

export default Login