import React, { useState } from 'react';
import { LockOutlined, UserOutlined } from '@ant-design/icons'
import { Button, Form, Input, message } from 'antd'
import { loginApi } from '../../../../services'
import { useNavigate } from 'react-router-dom'

interface Props {
  toggle: () => void;
}

interface FormValue {
  username: string;
  password: string;
}

const LoginForm: React.FC<Props> = (props) => {
  const [loading, setLoading] = useState(false)
  const navigate = useNavigate()

  const onFinish = async (values: FormValue) => {
    setLoading(true)
    try {
      const res = await loginApi(values)
      setLoading(false)
      if (res.data.code === 0) {
        message.success('登录成功')
        localStorage.setItem('token', res.data.token)
        navigate('/')
      } else {
        message.error(res.data.msg)
      }
    } catch(e) {
      console.log(e)
      message.error('网络错误，请稍后重试！')
    }
  };

  return (
    <Form
      style={{ maxWidth: 360, width: 340, border: '1px solid #dddddd', padding: 20, borderRadius: 5 }}
      onFinish={onFinish}
    >
      <Form.Item<FormValue>
        name="username"
        rules={[{ required: true, message: '请输入用户名!' }]}
      >
        <Input prefix={<UserOutlined />} placeholder="Username" />
      </Form.Item>
      <Form.Item<FormValue>
        name="password"
        rules={[{ required: true, message: '请输入密码!' }]}
      >
        <Input prefix={<LockOutlined />} type="password" placeholder="Password" />
      </Form.Item>

      <Form.Item>
        <Button block type="primary" htmlType="submit" loading={loading}>登录</Button>
        or <Button type="link" onClick={props.toggle}>去注册</Button>
      </Form.Item>
    </Form>
  );
}

export default LoginForm