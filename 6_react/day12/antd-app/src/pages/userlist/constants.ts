export enum Sex {
  men = 1,
  women = 0
}

export const SexText = {
  [Sex.men]: {
    color: '#f50',
    val: '男'
  },
  [Sex.women]: {
    color: '#2db7f5',
    val: '女'
  },
}