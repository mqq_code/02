import React from 'react';
import { Button, Form, Input, Space, Select } from 'antd';
import { UserItem } from '../../../types/api'

import style from './Search.module.scss'

export type FormVal = Partial<UserItem>

interface Props {
  onSearch: (value: FormVal) => void;
}

const Search: React.FC<Props> = (props) => {
  const [form] = Form.useForm<FormVal>()
  const onFinish = (value: FormVal) => {

    const params: FormVal = {}
    Object.keys(value).forEach((key: any) => {
      const v = value[key as keyof FormVal]
      if (v || typeof v === 'number') {
        params[key as keyof FormVal] = v
      }
    })
    // 通知父组件要搜索的内容
    props.onSearch(params)
  }
  const reset = () => {
    form.resetFields()
    props.onSearch({})
  }
  return (
    <div className={style.search}>
      <Form form={form} layout="inline" labelCol={{ span: 6 }} onFinish={onFinish}>
        <Form.Item name="username" label="姓名">
          <Input placeholder="Username" />
        </Form.Item>
        <Form.Item name="password" label="密码">
          <Input type="password" placeholder="Password" />
        </Form.Item>
        <Form.Item name="sex" label="性别">
          <Select
            placeholder="选择性别"
            allowClear
            options={[
              {
                value: 1,
                label: '男'
              },
              {
                value: 0,
                label: '女'
              }
            ]}
          ></Select>
        </Form.Item>
        <Form.Item name="age" label="年龄">
          <Input />
        </Form.Item>
        <Form.Item name="email" label="邮箱">
          <Input />
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 6 }}>
          <Space>
            <Button type="primary" htmlType="submit">搜索</Button>
            <Button onClick={reset}>重置</Button>
          </Space>
        </Form.Item>
      </Form>
    </div>
  )
}

export default Search