import React, { useState } from 'react'
import { EyeOutlined, EyeInvisibleOutlined } from '@ant-design/icons'
interface Props {
  password: string;
}


const Pass: React.FC<Props> = (props) => {
  const [show, setShow] = useState(false)
  return (
    <div style={{ display: 'flex', justifyContent: 'space-between' }}>
      <span>{show ? props.password : '***'}</span>
      <span onClick={() => setShow(!show)}>
        {show ? <EyeInvisibleOutlined /> : <EyeOutlined />}
      </span>
    </div>
  )
}

export default Pass