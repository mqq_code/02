import React, { useEffect, useState } from 'react'
import style from './UserList.module.scss'
import { userListApi, delUserApi, createUserApi, updateUserApi, exportApi } from '../../services'
import { UserItem, CreateParams } from '../../types/api'
import { ExclamationCircleFilled } from '@ant-design/icons'
import { Button, Space, Table, Tag, Modal, message,  Form, Input, InputNumber, Select  } from 'antd'
import type { TableProps } from 'antd'
import { SexText } from './constants'
import Pass from './components/Pass'
import Search, { type FormVal } from './components/Search'

interface FieldType {
  username: string;
  age: number;
  sex: 0 | 1;
  email: string;
  password: string;
}


const UserList: React.FC = () => {
  const [list, setList] = useState<UserItem[]>([])
  const [total, setTotal] = useState(0)
  const [params, setParams] = useState({ page: 1, pagesize: 5 })
  const [searchForm, setSearchForm] = useState<FormVal>({})
  const [visible, setVisible] = useState(false)
  const [updateId, setUpdateId] = useState<string | null>(null)
  const [updateNo, setUpdateNo] = useState<number | null>(null)
  const [form] = Form.useForm<CreateParams>()

  const getUserList = async () => {
    const res = await userListApi({
      ...params,
      ...searchForm
    })
    console.log(res.data.values.list)

    setList(res.data.values.list)
    setTotal(res.data.values.total)
  }

  useEffect(() => {
    getUserList()
  }, [params, searchForm])

  const del = (row: UserItem) => {
    Modal.confirm({
      title: '警告！',
      icon: <ExclamationCircleFilled />,
      content: <div>确定要删除 <b>{row.username}</b> 吗？</div>,
      cancelText: '取消',
      okText: '删除',
      okButtonProps: {
        danger: true
      },
      async onOk() {
        try {
          const res = await delUserApi(row.id)
          if (res.data.code === 0) {
            message.success('删除成功!')
            if (list.length === 1 && params.page === Math.ceil(total / params.pagesize)) {
              setParams({
                ...params,
                page: params.page - 1
              })
            } else {
              getUserList()
            }
          } else {
            message.error(res.data.msg)
          }
        } catch(e) {
          console.log(e)
        }
      }
    })
  }

  const columns: TableProps<UserItem>['columns'] = [
    {
      title: '序号',
      dataIndex: 'no',
      key: 'no',
      fixed: 'left'
    },
    {
      title: '用户id',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: '姓名',
      dataIndex: 'username',
      key: 'username',
    },
    {
      title: '年龄',
      dataIndex: 'age',
      key: 'age',
    },
    {
      title: '性别',
      dataIndex: 'sex',
      key: 'sex',
      render: (sex, record) => {
        return <Tag color={SexText[record.sex]?.color}>{SexText[record.sex]?.val}</Tag>
      }
    },
    {
      title: '邮箱',
      dataIndex: 'email',
      key: 'email',
    },
    {
      title: '密码',
      dataIndex: 'password',
      key: 'password',
      render: (_, record) => <Pass password={record.password} />
    },
    {
      title: '操作',
      key: 'action',
      fixed: 'right',
      render: (_, record) => (
        <Space size="middle">
          <Button size="small" onClick={() => {
            setVisible(true)
            setUpdateId(record.id)
            setUpdateNo(record.no)
            form.setFieldsValue({...record})
          }}>编辑</Button>
          <Button size="small" danger onClick={() => del(record)}>删除</Button>
        </Space>
      ),
    },
  ]

  const createUser = async (value: CreateParams) => {
    const res = await createUserApi(value)
    if (res.data.code === 0) {
      message.success('创建成功！')
      setVisible(false)
      getUserList()
    } else {
      message.error(res.data.msg)
    }
  }
  const updateUser = async (value: CreateParams) => {
    const res = await updateUserApi({
      ...value,
      id: updateId!,
      no: updateNo!
    })
    if (res.data.code === 0) {
      message.success('更新成功！')
      setVisible(false)
      getUserList()
    } else {
      message.error(res.data.msg)
    }
  }

  const handleOk = async () => {
    const value = await form.validateFields()
    if (updateId) {
      updateUser(value)
    } else {
      createUser(value)
    }
  }
  const handleCancel = () => {
    setVisible(false)
  }

  useEffect(() => {
    if (!visible) {
      form.resetFields()
      setUpdateId(null)
    }
  }, [visible])


  const onSearch = (value: FormVal) => {
    console.log('要搜索的内容', value)
    setSearchForm(value)
  }

  const exportExcel = async () => {
    const res = await exportApi(list.map(v => v.id))
    // 读取文件在内存中的地址
    const url = window.URL.createObjectURL(res.data)
    const a = document.createElement('a')
    a.href = url
    a.download = '文件名称.xlsx'
    document.body.appendChild(a)
    a.click()
    document.body.removeChild(a)
    // 释放内存
    window.URL.revokeObjectURL(url)
  }

  return (
    <div className={style.page}>
      <Search onSearch={onSearch} />
      <Space style={{ marginBottom: 20 }}>
        <Button type="primary" onClick={() => setVisible(true)}>新增</Button>
        <Button onClick={exportExcel}>导出</Button>
      </Space>
      <Table
        columns={columns}
        dataSource={list}
        rowKey="id"
        scroll={{ x: 1300 }}
        pagination={{
          current: params.page,
          pageSize: params.pagesize,
          pageSizeOptions: [5, 10, 15, 30],
          showQuickJumper: true,
          total,
          showTotal: (total, range) => {
            return (
              <Space>
                <span>共 {total} 条</span>
                <span>{range[0]} - {range[1]}</span>
              </Space>
            )
          },
          onChange: (page, pagesize) => {
            setParams({
              page,
              pagesize
            })
          }
        }}
      />
      <Modal title={updateId ? '编辑' : '新增'} open={visible} onOk={handleOk} onCancel={handleCancel} cancelText="取消" okText="确定">
        <Form
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 20 }}
          form={form}
        >
          <Form.Item<FieldType>
            label="姓名"
            name="username"
            rules={[{ required: true, message: '请输入用户名!' }]}
          >
            <Input />
          </Form.Item>

          <Form.Item<FieldType>
            label="密码"
            name="password"
            rules={[{ required: true, message: '请输入密码!' }]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item<FieldType>
            label="年龄"
            name="age"
            rules={[{ required: true, message: '请输入年龄!' }]}
          >
            <InputNumber min={18} max={58} />
          </Form.Item>
          <Form.Item<FieldType>
            label="性别"
            name="sex"
            rules={[{ required: true, message: '请选择性别!' }]}
          >
            <Select
              placeholder="选择性别"
              allowClear
              options={[
                {
                  value: 1,
                  label: '男'
                },
                {
                  value: 0,
                  label: '女'
                }
              ]}
            ></Select>
          </Form.Item>
          <Form.Item<FieldType>
            label="邮箱"
            name="email"
            rules={[
              { required: true, message: '请输入邮箱!' },
              { type: 'email', message: '邮箱格式错误!' }
            ]}
          >
            <Input />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  )
}

export default UserList