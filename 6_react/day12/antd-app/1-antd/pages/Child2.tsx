import React from 'react'
import { Space, Table, Tag, Button, Modal, message } from 'antd';
import type { TableProps } from 'antd';
import { ExclamationCircleFilled } from '@ant-design/icons';



interface DataType {
  id: string;
  name: string;
  age: number;
  address: string;
  tags: string[];
}


const Child2: React.FC = () => {


  const showConfirm = (row: DataType) => {
    Modal.confirm({
      title: '确定要删除吗?',
      icon: <ExclamationCircleFilled />,
      content: <div>姓名: <b style={{ color: 'red' }}>{row.name}</b></div>,
      onOk() {
        console.log('OK');
        message.success('删除成功')
      },
      onCancel() {
        console.log('Cancel');
        message.error('取消删除')
      },
    });
  };

  const columns: TableProps<DataType>['columns'] = [
    {
      title: '姓名',
      dataIndex: 'name',
      key: 'name',
      render: (name, record) => {
        console.log(record)
        return <div>
          <h3>{name}</h3>
          <p>描述111</p>
        </div>
      }
    },
    {
      title: '年龄',
      dataIndex: 'age',
      key: 'age',
    },
    {
      title: '地址',
      dataIndex: 'address',
      key: 'address',
    },
    {
      title: '标签',
      key: 'tags',
      dataIndex: 'tags',
      render: (_, { tags }) => (
        <>
          {tags.map((tag) => {
            let color = tag.length > 5 ? 'geekblue' : 'green';
            if (tag === 'loser') {
              color = 'volcano';
            }
            return (
              <Tag color={color} key={tag}>
                {tag.toUpperCase()}
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: '操作',
      key: 'action',
      render: (_, record) => {
        return (
            <Space size="middle">
            <Button size="small" type="primary">编辑 {record.name}</Button>
            <Button size="small" danger onClick={() => showConfirm(record)}>删除</Button>
          </Space>
        )
      },
    },
  ];
  
  const data: DataType[] = [
    {
      id: '1',
      name: '小明',
      age: 32,
      address: 'New York No. 1 Lake Park',
      tags: ['nice', 'developer'],
    },
    {
      id: '2',
      name: '阿红',
      age: 42,
      address: 'London No. 1 Lake Park',
      tags: ['loser'],
    },
    {
      id: '3',
      name: 'Joe Black',
      age: 32,
      address: 'Sydney No. 1 Lake Park',
      tags: ['cool', 'teacher'],
    },
  ];
  


  return (
    <div>
      <h2>Child2</h2>
      <Table columns={columns} dataSource={data} rowKey={row => row.id} />
    </div>
  )
}

export default Child2