import React from 'react'
import type { FormProps } from 'antd';
import { Button, Checkbox, Form, Input, Select, DatePicker } from 'antd';

type FieldType = {
  user?: string;
  pass?: string;
  remember?: string;
  gender: 'male' | 'female' | 'other';
};

const Child1: React.FC = () => {
  // 创建form实例
  const [formIns] = Form.useForm()

  const onFinish: FormProps<FieldType>['onFinish'] = (values) => {
    console.log('成功:', values);
  };
  
  const onFinishFailed: FormProps<FieldType>['onFinishFailed'] = (errorInfo) => {
    console.log('失败:', errorInfo);
  };

  const onSubmit = async () => {
    // console.log(formIns)
    // formIns.submit()
    const values = await formIns.validateFields()
    console.log('成功', values)

  }
  return (
    <div>
      <Form
        form={formIns}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        style={{ maxWidth: 600 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        
      >
        <Form.Item<FieldType>
          label="姓名"
          name="user"
          rules={[
            { required: true, message: '请输入用户名！' }
          ]}
        >
          <Input />
        </Form.Item>

        <Form.Item<FieldType>
          label="密码"
          name="pass"
          rules={[{ required: true, message: '请输入密码!' }]}
        >
          <Input.Password />
        </Form.Item>

        <Form.Item<FieldType> name="gender" label="性别" rules={[{ required: true, message: '请选择性别' }]}>
          <Select
            placeholder="请选择性别"
            allowClear
            options={[
              {
                label: '男',
                value: 'male'
              },
              {
                label: '女',
                value: 'female'
              }
            ]}
          >
          </Select>
        </Form.Item>


        <Form.Item name="date-picker" label="选择时间" rules={[
          { type: 'object' as const, required: true, message: 'Please select time!' }
        ]}>
          <DatePicker />
        </Form.Item>

        <Form.Item<FieldType>
          name="remember"
          valuePropName="checked"
          wrapperCol={{ offset: 8, span: 16 }}
        >
          <Checkbox>记住账号</Checkbox>
        </Form.Item>



        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" onClick={onSubmit}>Submit</Button>
        </Form.Item>
      </Form>
    </div>
  )
}

export default Child1