import React from 'react'
import { useRoutes } from 'react-router-dom'
import Child1 from './pages/Child1'
import Child2 from './pages/Child2'

const App: React.FC = () => {
  const routes = useRoutes([
    {
      path: '/',
      element: <Child1 />
    },
    {
      path: '/child2',
      element: <Child2 />
    }
  ])

  return routes
}

export default App