import React, { forwardRef, useImperativeHandle, useRef } from 'react'
import Question from '../question/Question'
import style from './QuestionsList.module.scss'

const QuestionsList = (props, ref) => {
  const mainRef = useRef(null)
  const questionsRef = useRef(null)

  const jump = i => {
    mainRef.current.scrollTop = questionsRef.current.children[i].offsetTop
  }

  useImperativeHandle(ref, () => {
    return {
      jump
    }
  }, [])

  return (
    <main ref={mainRef} className={style.main}>
      <div className={style.content}>
        <h2>单选题（共 {props.questions.length} 题，总分: {props.totalScore} 分 ）</h2>
        <div className={style.questions} ref={questionsRef}>
          {props.questions.map((item, index) =>
            <Question
              key={index}
              {...item}
              num={index + 1}
              onChange={props.onQuestionChange}
            />
          )}
        </div>
        <button onClick={props.onSubmit} disabled={props.total !== null}>{props.total === null ? '提交' : '已交卷'}</button>
      </div>
    </main>
  )
}

export default forwardRef(QuestionsList)