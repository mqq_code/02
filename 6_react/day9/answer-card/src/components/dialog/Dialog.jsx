import React from 'react'
import style from './Dialog.module.scss'
import { createPortal } from 'react-dom'

const Dialog = (props) => {
  if (!props.visible) return null

  const child = (
    <div className={style.dialog}>
      <div className={style.content}>
        <div className={style.center}>
          {props.children}
        </div>
        <div className={style.footer}>
          <button onClick={props.onConfirm}>确定</button>
        </div>
      </div>
    </div>
  )
  
  return createPortal(child, document.body)
}

export default Dialog