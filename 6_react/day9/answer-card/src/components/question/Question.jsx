import React from 'react'
import style from './Question.module.scss'

const letter = ['A', 'B', 'C', 'D']
const Question = (props) => {

  return (
    <div className={style.question}>
      <h3>
        <span>{props.num}</span>
        {props.question}
        ({props.score}分)
      </h3>
      <ul>
        {props.options.map((item, i) =>
          <li key={item}>
            <input
              type="radio"
              name={props.num}
              id={`${props.num}_${i}`}
              value={letter[i]}
              onChange={e => props.onChange(props.num, e.target.value)}
              disabled={props.disabled}
            />
            <label htmlFor={`${props.num}_${i}`}>{letter[i]}: {item}</label>
          </li>
        )}
      </ul>
      {props.isErr && <p style={{ color: 'red' }}>正确答案: {props.result}</p>}
    </div>
  )
}

export default Question