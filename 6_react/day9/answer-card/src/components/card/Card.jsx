import React, { useEffect } from 'react'
import style from './Card.module.scss'
import classNames from 'classnames'

const Card = (props) => {
  
  return (
    <div className={style.card}>
      <div className={style.title}>
        <b>答题卡</b>
        <span>{props.time}</span>
      </div>
      <div className={style.no}>
        {props.questions.map((v, i) =>
          <span
            key={i}
            className={classNames({ [style.active]: v.answer, [style.err]: v.isErr })}
            onClick={() => props.onClick(i)}
          >{i + 1}</span>
        )}
      </div>
    </div>
  )
}

export default Card