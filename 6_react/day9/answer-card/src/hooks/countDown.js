import { useState, useRef, useEffect } from "react"

const addZero = n => n >= 10 ? n : '0' + n

const format = (n) => {
  const h = Math.floor(n / 3600)
  const m = Math.floor(n % 3600 / 60)
  const s = n % 60
  return `${addZero(h)}:${addZero(m)}:${addZero(s)}`
}

export const useCountDown = (s, immediate) => {
  const [num, setNum] = useState(s)
  const timer = useRef(null)

  const start = () => {
    timer.current = setInterval(() => {
      setNum(n => {
        if (n === 1) {
          stop()
        }
        return n - 1
      })
    }, 1000)
  }

  const stop = () => {
    clearInterval(timer.current)
  }

  useEffect(() => {
    if (immediate) {
      start()
    }
    return () => {
      stop()
    }
  }, [])

  return {
    time: format(num),
    start,
    stop,
    num
  }
}