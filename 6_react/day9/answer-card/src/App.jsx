import { useEffect, useState, useRef, useMemo } from 'react'
import style from './App.module.scss'
import Card from './components/card/Card'
import Dialog from './components/dialog/Dialog'
import QuestionsList from './components/question-list/QuestionsList'
import { useCountDown } from './hooks/countDown'

const App = () => {
  // 倒计时
  const { time, num, stop } = useCountDown(60, true)
  const QuestionsListRef = useRef(null)
  const [total, setTotal] = useState(null)
  const [questions, setQuestions] = useState([])
  const [visible, setVisible] = useState(false)

  const getData = async () => {
    const res = await fetch('https://zyxcl.xyz/exam_api/exan_data').then(res => res.json())
    console.log(res)
    setQuestions(res)
  }
  useEffect(() => {
    getData()
  }, [])

  // 监听倒计时结束自动提交
  useEffect(() => {
    if (num === 0) {
      submit()
    }
  }, [num])

  // 选择题目
  const questionChange = (no, val) => {
    const newQuestions = [...questions]
    newQuestions[no - 1].answer = val
    setQuestions(newQuestions)
  }

  // 跳转题目位置
  const jump = i => {
    // 调用子组件内的方法
    QuestionsListRef.current.jump(i)
  }

  // 计算试卷满分
  const totalScore = useMemo(() => {
    return questions.reduce((prev, val) => {
      return prev + val.score
    }, 0)
  }, [questions])

  // 提交试卷
  const submit = () => {
    const newQuestions = [...questions]
    newQuestions.forEach(item => {
      if (item.answer !== item.result) {
        item.isErr = true
      }
      item.disabled = true
    })
    setQuestions(newQuestions)
    setTotal(questions.reduce((prev, val) => {
      return prev + (val.answer === val.result ? val.score : 0)
    }, 0))
    setVisible(true)
    stop()
  }

  return (
    <div className={style.app}>
      <QuestionsList
        ref={QuestionsListRef}
        totalScore={totalScore}
        questions={questions}
        total={total}
        onSubmit={submit}
        onQuestionChange={questionChange}
      />
      <Card questions={questions} time={time} onClick={jump} />
      <Dialog
        visible={visible}
        onConfirm={() => setVisible(false)}
      >
        <h2>分数：{total}</h2>
      </Dialog>
    </div>
  )
}

export default App