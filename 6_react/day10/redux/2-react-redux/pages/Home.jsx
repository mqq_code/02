import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'

const Home = () => {
  const dispatch = useDispatch()
  const xm = useSelector(state => ({
    name: state.name,
    age: state.age
  }))
  const banners = useSelector(state => state.banners)

  useEffect(() => {
    fetch('https://zyxcl.xyz/music/api/banner')
      .then(res => res.json())
      .then(res => {
        dispatch({
          type: 'set_banenrs',
          payload: res.banners
        })
      })
  }, [])

  return (
    <div>
      <h1>Home</h1>
      <p>姓名: {xm.name}</p>
      <p>年龄: {xm.age}</p>
      <button onClick={() => {
        dispatch({
          type: 'change_age',
          payload: 2
        })
      }}>age+</button>

      <ul>
        {banners.map(it =>
          <li key={it.targetId}>
            <img src={it.imageUrl} width={300} alt="" />
          </li>
        )}
      </ul>
    </div>
  )
}

export default Home