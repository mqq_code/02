import { createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger'

const initState = {
  name: '小明',
  age: 22,
  banners: []
}

// 返回最新的state
const reducer = (state = initState, action) => {
  // state: 最新的state
  // action: 描述本次如何修改的对象
  // console.log(state, action)
  if (action.type === 'change_age') {
    return {...state, age: state.age + action.payload}
  } else if (action.type === 'set_banenrs') {
    return {...state, banners: action.payload}
  }
  return state
}

// applyMiddleware: 添加中间件
// 中间件：增强 dispatch，给 dispatch 添加额外的功能
const store = createStore(reducer, applyMiddleware(logger))

export default store