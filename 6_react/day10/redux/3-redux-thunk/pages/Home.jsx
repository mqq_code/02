import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { setBannerAction, setAgeAction } from '../actions'

const Home = () => {
  const dispatch = useDispatch()
  const xm = useSelector(state => ({
    name: state.name,
    age: state.age
  }))
  const banners = useSelector(state => state.banners)

  useEffect(() => {
    dispatch(setBannerAction)
  }, [])

  return (
    <div>
      <h1>Home</h1>
      <p>姓名: {xm.name}</p>
      <p>年龄: {xm.age}</p>
      <button onClick={() => {
        dispatch(setAgeAction(2))
      }}>age+</button>

      <ul>
        {banners.map(it =>
          <li key={it.targetId}>
            <img src={it.imageUrl} width={300} alt="" />
          </li>
        )}
      </ul>
    </div>
  )
}

export default Home

