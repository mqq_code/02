import React, { Component } from 'react'
import { connect } from 'react-redux'
import { setBannerAction, setAgeAction } from '../actions'

class About extends Component {

  add = () => {
    this.props.dispatch(setAgeAction(1))

  }
  componentDidMount() {
    this.props.dispatch(setBannerAction)
  }
  render() {

    return (
      <div>
        <h1>About</h1>
        <p>age: {this.props.age}</p>
        <button onClick={this.add}>age+1</button>
        <ul>
        {this.props.banners.map(it =>
          <li key={it.targetId}>
            <img src={it.imageUrl} width={300} alt="" />
          </li>
        )}
      </ul>
      </div>
    )
  }
}

const mapStateToProps = state => {
  // console.log('store中的state', state)
  // return的数据会添加到组件的props中
  return {
    age: state.age,
    banners: state.banners
  }
}
// connect: 高阶组件，连接组件和store，把store中的数据传给组件
export default connect(mapStateToProps)(About)