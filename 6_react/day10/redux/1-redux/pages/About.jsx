import React, { Component } from 'react'
import store from '../store'


class About extends Component {

  state = {
    age: store.getState().age
  }
  unsub = null
  componentDidMount() {
    this.unsub = store.subscribe(() => {
      console.log('about', store.getState())
      this.setState({
        age: store.getState().age
      })
    })
  }
  componentWillUnmount() {
    this.unsub()
  }
  add = () => {
    store.dispatch({
      type: 'change_age',
      payload: 1
    })
  }
  render() {
    return (
      <div>
        <h1>About</h1>
        <p>age: {this.state.age}</p>
        <button onClick={this.add}>age+1</button>
      </div>
    )
  }
}


export default About