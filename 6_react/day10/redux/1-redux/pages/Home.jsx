import React, { useEffect, useState } from 'react'
import store from '../store'

console.log(store)
// 获取store数据
// console.log(store.getState())

const Home = () => {
  const [state, setState] = useState(store.getState())

  useEffect(() => {
    const unsubscribe =  store.subscribe(() => {
      console.log('home页面 store中有数据改变了', store.getState())
      setState(store.getState())
    })

    return () => {
      // 销毁监听
      unsubscribe()
    }
  }, [])

  useEffect(() => {
    fetch('https://zyxcl.xyz/music/api/banner')
      .then(res => res.json())
      .then(res => {
        store.dispatch({
          type: 'set_banenrs',
          payload: res.banners
        })
      })
  }, [])

  return (
    <div>
      <h1>Home</h1>
      <p>姓名: {state.name}</p>
      <p>年龄: {state.age}</p>
      <button onClick={() => {
        // 通过 dispatch 触发 reducer函数执行，修改state
        // action: 一个有type属性的对象
        store.dispatch({
          type: 'change_age',
          payload: 2
        })
      }}>age+</button>

      <ul>
        {state.banners.map(it =>
          <li key={it.targetId}>
            <img src={it.imageUrl} width={300} alt="" />
          </li>
        )}
      </ul>
    </div>
  )
}

export default Home