import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { addAge, changeName } from '../store/features/userSlice'

const Home = () => {
  const dispatch = useDispatch()
  const age = useSelector(state => state.user.age)
  const title = useSelector(state => state.user.title)


  const add = () => {
    dispatch(addAge(1))
  }
  return (
    <div>
      <h1>Home</h1>
      <h2>{title}</h2>
      <input type="text" value={title} onChange={e => {
        dispatch(changeName(e.target.value))
      }} />
      <p>年龄：{age}</p>
      <button onClick={add}>age+</button>
    </div>
  )
}

export default Home