import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { setBanners, bannerApi } from '../store/features/homeSlice'

const About = () => {
  const banners = useSelector(state => state.home.banners)
  const loading = useSelector(state => state.home.loading)

  const dispatch = useDispatch()

  useEffect(() => {

    dispatch(bannerApi(123))

  }, [])



  return (
    <div>
      <h2>about</h2>
      {JSON.stringify(banners)}
      {loading && <div className='loading'>加载中</div>}
    </div>
  )
}

export default About