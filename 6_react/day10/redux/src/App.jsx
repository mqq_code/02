import React from 'react'
import { useRoutes, NavLink } from 'react-router-dom'
import Home from './pages/Home'
import About from './pages/About'

const App = () => {
  const routes = useRoutes([
    {
      path: '/about',
      element: <About />
    },
    {
      path: '/',
      element: <Home />
    }
  ])
  return (
    <div>
      <nav>
        <NavLink to="/">首页</NavLink>
        <NavLink to="/about">about</NavLink>
      </nav>
      {routes}
    </div>
  )
}

export default App