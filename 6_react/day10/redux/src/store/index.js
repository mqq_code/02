import { configureStore } from '@reduxjs/toolkit'
import userReducer from './features/userSlice'
import homeReducer from './features/homeSlice'

const store = configureStore({
  reducer: {
    user: userReducer,
    home: homeReducer
  }
})

export default store