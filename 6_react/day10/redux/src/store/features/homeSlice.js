import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'

// 创建异步action
export const bannerApi = createAsyncThunk('home/bannerApi', async (a) => {
  console.log('执行了 bannerApi', a)
  const res = await fetch('https://zyxcl.xyz/music/api/banner').then(res => res.json())
  // return 的数据会传给 fulfilled 状态中的 action.payload
  return res.banners
})


export const topListApi = createAsyncThunk('home/topListApi', async (a) => {
  const res = await fetch('https://zyxcl.xyz/music/api/hotlist').then(res => res.json())
  return res.banners
})


const homeSlice = createSlice({
  name: 'home',
  initialState: {
    banners: [],
    loading: false
  },
  reducers: {
    setBanners(state, action) {
      state.banners = action.payload
    }
  },
  extraReducers: builder => {
    builder
      .addCase(bannerApi.pending, (state, action) => {
        console.log('正在调用banner接口', state, action)
        state.loading = true
      })
      .addCase(bannerApi.fulfilled, (state, action) => {
        console.log('调用 banner 接口成功', state, action)
        state.banners = action.payload
        state.loading = false
      })
      .addCase(bannerApi.rejected, (state, action) => {
        console.log('调用 banner 接口失败', state, action)
      })
      .addCase(topListApi.fulfilled, () => {
        
      })
  }
})

// 同步action
export const { setBanners } = homeSlice.actions
export default homeSlice.reducer
