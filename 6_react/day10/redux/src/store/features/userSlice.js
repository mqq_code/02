import { createSlice } from '@reduxjs/toolkit'

const userSlice = createSlice({
  name: 'user',
  initialState: {
    title: 'xm',
    age: 22
  },
  reducers: {
    addAge(state, action) {
      console.log(action)
      state.age += action.payload
    },
    changeName(state, action) {
      state.title = action.payload
    }
  }
})

export const { addAge, changeName } = userSlice.actions
export default userSlice.reducer
