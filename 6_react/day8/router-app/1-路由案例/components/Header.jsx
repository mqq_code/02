import React, { useEffect, useState } from 'react'
import { allList } from './constants'
import { NavLink, useLocation, useNavigate } from 'react-router-dom'

const Header = () => {
  const [navList, setNavList] = useState([])
  const location = useLocation()
  const navigate = useNavigate()

  useEffect(() => {
    if (!navList.find(v => v.path === location.pathname)) {
      const val = allList.find(v => v.path === location.pathname)
      if (val) {
        setNavList([...navList, val])
      }
    }
  }, [location.pathname])

  const remove = (path, index, e) => {
    e.preventDefault()
    const newList = [...navList]
    if (navList.length === 1) {
      setNavList([])
      navigate('/login')
      return
    }
    if (path === location.pathname) {
      if (index === navList.length -1) {
        navigate(navList[index - 1].path)
        newList.splice(index, 1)
      } else {
        navigate(navList[index + 1].path)
        newList.splice(index, 1)
      }
    } else {
      newList.splice(index, 1)
    }
    setNavList(newList)

  }

  return (
    <div className='header'>
      {navList.map((item, index) =>
        <NavLink key={item.path} to={item.path}>
          {item.name}
          <span onClick={(e) => remove(item.path, index,  e)}>x</span>
        </NavLink>
      )}
    </div>
  )
}

export default Header