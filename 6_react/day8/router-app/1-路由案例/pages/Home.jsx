import React from 'react'
import { Outlet, NavLink } from 'react-router-dom'
import Menu from '../components/Menu'
import Header from '../components/Header'

const Home = () => {
  return (
    <div className='home'>
      <Menu />
      <div className="main">
        <Header />
        <div className="content">
          <Outlet />
        </div>
      </div>
    </div>
  )
}

export default Home