import React from 'react'
import { Navigate, useLocation } from 'react-router-dom'

const Auth = (props) => {

  const token = localStorage.getItem('token')
  const location = useLocation()

  if (props.isAuth) {
    if (!token) {
      return <Navigate to={`/login?redirectUrl=${encodeURIComponent(location.pathname)}`} />
    }
  }
  
  return props.children
}

export default Auth