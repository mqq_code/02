import { Navigate } from 'react-router-dom'
import Auth from '../hoc/Auth'
import Home from '../pages/Home'
import Login from '../pages/Login'
import Child1 from '../pages/Child1'
import Child2 from '../pages/Child2'
import Child3 from '../pages/Child3'
import Child4 from '../pages/Child4'
import Child5 from '../pages/Child5'

const routes = [
  {
    path: '/',
    element: <Navigate to="/home" />
  },
  {
    path: '/home',
    element: <Home />,
    isAuth: true,
    title: '首页',
    children: [
      {
        path: '/home',
        element: <Navigate to="/home/child1" />
      },
      {
        path: '/home/child1',
        element: <Child1 />,
        title: 'child1',
      },
      {
        path: '/home/child2',
        element: <Child2 />,
        title: 'child2',
      },
      {
        path: '/home/child3',
        element: <Child3 />,
        title: 'child3',
      },
      {
        path: '/home/child4',
        element: <Child4 />,
        title: 'child4',
      },
      {
        path: '/home/child5',
        element: <Child5 />,
        title: 'child5',
      }
    ]
  },
  {
    path: '/login',
    element: <Login />,
    title: '登录',
  },
  {
    path: '*',
    element: <div>404</div>
  }
]

const RouteRender = (props) => {
  document.title = props.title
  return props.children
}


const formatRoutes = (routes) => {
  if (!Array.isArray(routes)) return []
  
  return routes.map(item => {
    return {
      ...item,
      element: (
        <RouteRender title={item.title}>
          <Auth isAuth={item.isAuth}>
            {item.element}
          </Auth>
        </RouteRender>
      ),
      children: formatRoutes(item.children)
    }
  })
}



export default formatRoutes(routes)