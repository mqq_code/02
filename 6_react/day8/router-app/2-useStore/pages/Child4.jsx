import React, { useState, Suspense } from 'react'
import { useStore } from '../store/index'

const Child4 = () => {
  const { state, dispatch } = useStore()

  console.log(state)

  return (
    <div>
      <h2>Child4</h2>
      <p>{JSON.stringify(state)}</p>
      <p>{state.count}</p>
      <button onClick={() => {
        // 执行reducer函数
        dispatch({
          type: 'CHANGE_COUNT',
          payload: 2
        })
      }}>count+</button>
    </div>
  )
}

export default Child4