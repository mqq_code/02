import React from 'react'
import { Link } from 'react-router-dom'

const Child2 = () => {
  return (
    <div>
      <h2>Child2</h2>
    </div>
  )
}

export default Child2