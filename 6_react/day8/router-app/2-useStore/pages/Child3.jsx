import React, { useState, Suspense, useRef } from 'react'
import { useForceUpdate } from '../hooks/useForceUpdate'

const Child3 = () => {
  const forceUpdate = useForceUpdate()
  const num = useRef(1)

  console.log(num.current)

  return (
    <div>
      <h2>Child3</h2>
      <p>num: {num.current}</p>
      <button onClick={() => {
        num.current ++
        // console.log(num.current)
        forceUpdate()
      }}>+</button>
    </div>
  )
}

export default Child3