import React, { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'

const Child1 = () => {
  const [playlist, setPlaylist] = useState([])

  useEffect(() => {
    fetch('https://zyxcl.xyz/music/api/top/playlist/highquality')
      .then(res => res.json())
      .then(res => {
        console.log(res.playlists)
        setPlaylist(res.playlists)
      })
  }, [])

  return (
    <div className='child'>
      {playlist.map(item =>
        <div key={item.id}>
          <h3>{item.name}</h3>
          <img src={item.coverImgUrl} width={100} alt="" />
          <p>{item.description}</p>
        </div>
      )}
    </div>
  )
}

export default Child1