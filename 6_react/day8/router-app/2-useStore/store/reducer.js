
export const initState = {
  count: 10,
  name: 'xiaoming',
  age: 22,
  hobby: ['吃饭']
}

// 修改state数据
export const reducer = (state, action) => {
  // console.log('state:', state)
  // console.log('action: ', action)
  if (action.type === 'CHANGE_COUNT') {
    return {
      ...state,
      count: state.count + action.payload
    }
  } else if (action.type === 'CHANGE_NAME') {
    return {
      ...state,
      name: action.payload
    }
  }
  return state
}