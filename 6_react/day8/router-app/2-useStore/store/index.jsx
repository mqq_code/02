import { createContext, useContext, useReducer, useState } from 'react'
import { initState, reducer } from './reducer'

export const store = createContext()


export const Provider = (props) => {
  const [state, dispatch] = useReducer(reducer, initState)
  return (
    <store.Provider value={{ state, dispatch }}>
      {props.children}
    </store.Provider>
  )
}


export const useStore = () => useContext(store)