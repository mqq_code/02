import React, { useEffect, useLayoutEffect, useMemo, useState } from 'react'
import { allList } from './constants'
import classNames from 'classnames'
import { NavLink, useLocation, useNavigate } from 'react-router-dom'


const formatTabData = (list) => {
  const res = []
  list.forEach(item => {
    const curTab = res.find(v => v.title === item.title)
    if (curTab) {
      curTab.list.push(item)
    } else {
      res.push({
        title: item.title,
        list: [item]
      })
    }
  })

  return res
}


const Menu = () => {
  const [tabList] = useState(formatTabData(allList))
  const [curIndex, setCurIndex] = useState(0)
  const location = useLocation()
  const navigate = useNavigate()

  const title = useMemo(() => {
    return allList.find(v => v.path === location.pathname)?.name
  }, [location.pathname])

  useLayoutEffect(() => {
    tabList.forEach((item, index) => {
      const val = item.list.find(v => v.path === location.pathname)
      if (val) {
        setCurIndex(index)
      }
    })
  }, [location.pathname])

  return (
    <div className='menu'>
      <div className="menu-tab">
        {tabList.map((item, index) =>
          <p
            key={item.title}
            className={classNames({ active: curIndex === index })}
            onClick={() => {
              setCurIndex(index)
              navigate(tabList[index].list[0].path)
            }}
          >{item.title}</p>
        )}
      </div>
      <div className="menu-con">
        <h3>{title}</h3>
        <ul>
          {tabList[curIndex].list.map(item =>
            <li key={item.path}>
              <NavLink to={item.path}>{item.name}</NavLink>
            </li>
          )}
        </ul>
      </div>
    </div>
  )
}

export default Menu