import React, { Component } from 'react'
import withPos from '../hoc/withPos'

class Child2 extends Component {
  state = {
    count: 0
  }

  render() {
    console.log(this.props)
    return (
      <div className='box'>
        <h2>Child2</h2>
        <p>{this.props.text}</p>
        <p>鼠标位置 {JSON.stringify(this.props.pos)}</p>
        <button>按钮</button>
        {this.state.count}
      </div>
    )
  }
}

// 调用高阶组件给 Child2 添加公共逻辑
export default withPos(Child2)