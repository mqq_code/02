import React, { Component } from 'react'
import withPos from '../hoc/withPos'

class Child1 extends Component {

  state = {
    title: 'Child1组件'
  }

  render() {
    return (
      <div className='box'>
        <p>鼠标位置： {JSON.stringify(this.props.pos)}</p>
        <h2>{this.state.title}</h2>
        <ul>
          <li>1</li>
          <li>2</li>
          <li>3</li>
          <li>4</li>
          <li>5</li>
          <li>6</li>
          <li>7</li>
          <li>8</li>
          <li>9</li>
          <li>10</li>
        </ul>
      </div>
    )
  }
}

export default withPos(Child1)
