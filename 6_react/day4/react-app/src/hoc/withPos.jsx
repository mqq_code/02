import { Component } from 'react'


function withPos(Com) {

  class Pos extends Component {
    state = {
      pos: {
        x: 0,
        y: 0
      }
    }
  
    mousemove = e => {
      this.setState({
        pos: {
          x: e.pageX,
          y: e.pageY
        }
      })
    }
  
    componentDidMount() {
      document.addEventListener('mousemove', this.mousemove)
    }
  
    componentWillUnmount() {
      document.removeEventListener('mousemove', this.mousemove)
    }
    render() {
      return (
        <Com pos={this.state.pos} {...this.props} />
      )
    }
  }
  
  return Pos
}

export default withPos