import React, { useEffect, useRef, useState } from 'react'
import style from './App.module.scss'
import Header from './components/header/Header'
import SearchDefault from './components/searchDefault/SearchDefault'
import SearchResult from './components/searchResult/SearchResult'
import SearchSuggest from './components/searchSuggest/SearchSuggest'
import {
  searchSuggestApi,
  searchApi
} from './services'

const App = () => {
  // 当前展示的页面
  const [curPage, setCurPage] = useState(0)// 0: SearchDefault, 1: SearchSuggest, 2: SearchResult
  // 历史记录列表
  const [searchHistory, setSearchHistory] = useState(JSON.parse(localStorage.getItem('searchHistory')) || [])
  // 搜索建议列表
  const [suggestList, setSuggestList] = useState([])
  // 搜索结果列表
  const [songs, setSongs] = useState([])
  // 搜索结果是否存在下一页
  const [hasMore, setHasMore] = useState(true)
  // 搜索内容，传给接口的keywords
  const [searchText, setSearchText] = useState('')
  // 搜索建议防抖的延时器
  const suggestTimer = useRef(null)
  // 获取 Header 组件的方法
  const HeaderRef = useRef(null)

  // 搜索建议
  const getSuggest = async val => {
    const res = await searchSuggestApi(val)
    setSuggestList(res.result?.allMatch || [])
  }

  // 输入框输入内容，展示搜索建议
  const onInput = (val) => {
    setCurPage(1)
    setSearchText(val)
    // 添加防抖
    if (suggestTimer.current) clearTimeout(suggestTimer.current)
    suggestTimer.current = setTimeout(() => {
      getSuggest(val)
    }, 500)
  }

  // 没有搜索内容，跳转到默认页面
  const onChange = (val) => {
    if (val.length === 0) {
      // 跳转到默认页面，歌曲列表置空
      setSuggestList([])
      setSongs([])
      setCurPage(0)
    }
  }



  // 搜索结果页面功能
  // 调用搜索接口
  const getSearchResult = async (offset, keyword) => {
    const res = await searchApi({
      keywords: keyword || searchText,
      limit: 30,
      offset
    })
    if (offset === 0) {
      setSongs(res.result.songs)
    } else {
      setSongs(songs.concat(res.result.songs))
    }
    setHasMore(res.result.hasMore)
  }
  
  // 开始搜索
  const startSearch = async val => {
    setCurPage(2) // 展示搜索结果页面
    HeaderRef.current.setSearchText(val) // 把要搜索的内容添加到input中
    addHistory(val) // 添加历史记录
    setSearchText(val) // 存要搜索的内容
    getSearchResult(0, val) // 开始搜索
  }

  // 存历史记录
  const addHistory = val => {
    const index = searchHistory.indexOf(val)
    const arr = [...searchHistory]
    if (index > -1) {
      arr.splice(index, 1)
    } else if (searchHistory.length === 10) {
      arr.pop()
    }
    arr.unshift(val)
    setSearchHistory(arr)
  }

  // 搜索结果页面滚动到底部时加载下一页
  const nextPage = () => {
    console.log('下一页', hasMore)
    if (hasMore) {
      getSearchResult(songs.length)
    }
  }

  // 监听历史记录改变，存到本地存储
  useEffect(() => {
    localStorage.setItem('searchHistory', JSON.stringify(searchHistory))
  }, [searchHistory])

  // 渲染的页面
  const renderPage = () => {
    if (curPage === 0) {
      return <SearchDefault searchHistory={searchHistory} onSearch={startSearch} onClear={() => setSearchHistory([])} />
    } else if (curPage === 1) {
      return <SearchSuggest list={suggestList} searchText={searchText} onSearch={startSearch} />
    } else if (curPage === 2) {
      return <SearchResult songs={songs} onLoadNextPage={nextPage} />
    } 
  }

  return (
    <div className={style.app}>
      <Header
        ref={HeaderRef}
        onChange={onChange}
        onInput={onInput}
        onSearch={startSearch}
      />
      <div className={style.content}>
        {renderPage()}
      </div>
    </div>
  )
}

export default App