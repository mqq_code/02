

export const searchHotApi = () => {
  return fetch('https://zyxcl.xyz/music/api/search/hot/detail').then(res => res.json())
}

export const searchSuggestApi = (keywords) => {
  return fetch(`https://zyxcl.xyz/music/api/search/suggest?keywords=${keywords}&type=mobile`).then(res => res.json())
}

export const searchApi = ({keywords, limit = 10, offset = 0}) => {
  return fetch(`https://zyxcl.xyz/music/api/search?keywords=${keywords}&limit=${limit}&offset=${offset}`).then(res => res.json())
}