import React from 'react'
import style from './SearchResult.module.scss'

const SearchResult = (props) => {

  const onScroll = e => {
    const { scrollTop, clientHeight, scrollHeight } = e.target
    console.log(scrollTop + clientHeight, scrollHeight)
    if (scrollTop + clientHeight + 10 >= scrollHeight) {
      props.onLoadNextPage()
    }
  }
  return (
    <div className={style.wrap} onScroll={onScroll}>
      {props.songs.map(item =>
        <div className={style.song} key={item.id}>
          <h3>{item.name}</h3>
          <div>
            歌手: {item.artists.map(v => v.name).join('/')}
          </div>
        </div>
      )}
    </div>
  )
}

export default SearchResult