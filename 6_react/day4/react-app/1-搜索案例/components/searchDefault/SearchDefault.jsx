import React, { useEffect, useState } from 'react'
import style from './SearchDefault.module.scss'
import { searchHotApi } from '../../services'
import classNames from 'classnames'

const SearchDefault = (props) => {
  const [hotList, setHotList] = useState([])

  const getList = async () => {
    const res = await searchHotApi()
    setHotList(res.data)
  }

  useEffect(() => {
    getList()
  }, [])
  
  return (
    <div className={style.wrap}>
      {props.searchHistory.length > 0 &&
        <>
          <div className={style.title}>搜索历史 <span onClick={props.onClear}>清空</span></div>
          <ol>
            {props.searchHistory.map(v =>
              <li key={v} onClick={() => props.onSearch(v)}>{v}</li>
            )}
          </ol>
        </>
      }
      <div className={style.title}>热门搜索</div>
      <ul>
        {hotList.map((item, index) => 
          <li
            key={item.searchWord}
            className={classNames({ [style.hl]: index < 3 })}
            onClick={() => props.onSearch(item.searchWord)}
          >
            <span>{index + 1}.</span>
            {item.searchWord}
            {item.iconUrl && <img src={item.iconUrl} />}
          </li>
        )}
      </ul>
    </div>
  )
}

export default SearchDefault