import React, { useEffect, useState, forwardRef, useImperativeHandle } from 'react'
import style from './Header.module.scss'

const Header = (props, ref) => {

  const [searchText, setSearchText] = useState('')
  const onChange = e => {
    setSearchText(e.target.value.trim())
    // 通知父组件正在输入
    props.onInput(e.target.value.trim(), e)
  }
  
  useEffect(() => {
    // 监听数据框改变通知父组件
    props.onChange(searchText)
  }, [searchText])

  useImperativeHandle(ref, () => {
    return {
      setSearchText
    }
  }, [])
  
  return (
    <div className={style.header}>
      <div className={style.input}>
        <input
          type="text"
          placeholder='请输入搜索的歌手/歌曲'
          value={searchText}
          onChange={onChange}
          onKeyDown={e => {
            if (e.keyCode === 13) {
              props.onSearch(searchText)
            }
          }}
        />
        {searchText && <span onClick={() => setSearchText('')}>x</span>}
      </div>
    </div>
  )
}

export default forwardRef(Header)