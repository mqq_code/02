import React from 'react'
import style from './SearchSuggest.module.scss'

const SearchSuggest = (props) => {

  const format = v => {
    const reg = new RegExp(props.searchText, 'gi')
    return v.replaceAll(reg, $0 => {
      return `<span style="color: red">${$0}</span>`
    })
  }

  return (
    <div className={style.wrap}>
      {props.list.length === 0 ?
        <div>没有匹配内容</div>
      :
        <ul>
          {props.list.map(item =>
            <li
              key={item.keyword}
              onClick={() => props.onSearch(item.keyword)}
              dangerouslySetInnerHTML={{ __html: format(item.keyword) }}
            >
            </li>
          )}
        </ul>
      }
    </div>
  )
}

export default SearchSuggest