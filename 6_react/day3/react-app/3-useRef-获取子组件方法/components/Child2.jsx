import React, { useState, forwardRef, useImperativeHandle } from 'react'

const Child2 = (props, parentRef) => {
  const [num, setNum] = useState(0)
  const [flag, setFlag] = useState(true)

  const change = n => {
    setNum(num + n)
  }
  // 给传入的ref对象设置内容
  useImperativeHandle(parentRef, () => {
    // 把return的内容赋值给传入的ref对象
    console.log('给ref赋值', flag)
    return {
      num,
      change
    }
  }, [num])

  return (
    <div style={{ border: '1px solid', padding: '20px' }}>
      <h2>Child2 函数组件</h2>
      <button onClick={() => change(1)}>+</button>
      {num}
      <hr />
      <button onClick={() => setFlag(!flag)}>{JSON.stringify(flag)}</button>
    </div>
  )
}

// forwardRef：转发父组件传入的ref到当前组件的高阶组件
export default forwardRef(Child2)