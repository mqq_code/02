import React, { useLayoutEffect, useEffect, useState } from 'react'

const App = () => {
  const [count, setCount] = useState(0)

  // useEffect(() => {
  //   // 页面渲染完成之后执行此函数
  //   // console.log(8, document.querySelector('p').outerHTML)
  //   if (count === 'aflkjadslfjkalsfjlasdjflkadsjflkadsjflkjadslkfjdaslkjfadlks') {
  //     setCount(Math.random())
  //   }
  // }, [count])


  useLayoutEffect(() => {
    // 页面渲染之前执行，可以避免页面闪烁
    // console.log(13, document.querySelector('p').outerHTML)
    if (count === 'aflkjadslfjkalsfjlasdjflkadsjflkadsjflkjadslkfjdaslkjfadlks') {
      setCount(Math.random())
    }
  }, [count])

  return (
    <div>
      <h1>App</h1>
      {count === 'aflkjadslfjkalsfjlasdjflkadsjflkadsjflkjadslkfjdaslkjfadlks' ?
        <div style={{ background: 'red', height: '400px' }}>{count}</div>
      :
        <p>{count}</p>
      }
      <button onClick={() => setCount(count + 1)}>+</button>
      <button onClick={() => setCount('aflkjadslfjkalsfjlasdjflkadsjflkadsjflkjadslkfjdaslkjfadlks')}>修改count</button>
    </div>
  )
}

export default App