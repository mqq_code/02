import React, { useEffect, useState } from 'react'
import style from './App.module.scss'
import Child1 from './components/Child1'
import { Provider } from './ctx/count'


const App = () => {
  const [count, setCount] = useState(0)


  const ctxContent = {
    count,
    setCount,
    a: 100,
    b: 'bbbb'
  }
  
  return (
    // 提供数据给所有后代组件
    <Provider value={ctxContent}>
      <div>
        <h1>app _  {count}</h1>
        <button onClick={() => setCount(count + 1)}>+</button>
        <Child1 />
      </div>
    </Provider>
  )
}

export default App