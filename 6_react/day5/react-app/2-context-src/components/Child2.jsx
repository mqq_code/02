import React, { useContext } from 'react'
import Child3 from './Child3'
import countCtx from '../ctx/count'
import textCtx from '../ctx/text'

const Child2 = () => {
  const countVal = useContext(countCtx)
  const textVal = useContext(textCtx)

  // console.log(countVal)
  // console.log(textVal)

  return (
    <div className='box'>
      <h2>Child2 - {textVal}</h2>
      <p>count: {countVal.count}</p>
      <Child3 />
    </div>
  )
}

export default Child2