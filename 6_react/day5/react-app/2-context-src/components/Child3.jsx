import React, { Component } from 'react'
import { Consumer } from '../ctx/count'
import { Consumer as TextConsumer } from '../ctx/text'


class Child3 extends Component {

  renderCtx = value => {
    // value: Provider传入的数据
    // console.log(value)
    return (
      <TextConsumer>
        {text => (
          <div className='box'>
            <h2>Child3 - {text}</h2>
            <p>App的count: {value.count}</p>
            <button onClick={() => value.setCount(c => c - 1)}>-</button>
          </div>
        )}
      </TextConsumer>
    )
  }

  render() {
    return (
      <Consumer>
        {this.renderCtx}
      </Consumer>
    )
  }
}

export default Child3
