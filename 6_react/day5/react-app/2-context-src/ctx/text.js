import React from 'react'


const ctx = React.createContext()
export const Provider = ctx.Provider
export const Consumer = ctx.Consumer

export default ctx