import React, { useLayoutEffect, useEffect, useState, useReducer, act } from 'react'


// useReducer: 定义组件状态
// useReducer(reducer, initVal)
// reducer: 修改和返回state的函数

const reducer = (state, action) => {
  console.log('调用了reducer函数')
  console.log('最新的state', state)
  console.log('本次的action', action)

  if (action.type === 'change_age') {
    return {...state, age: state.age + 1} 
  } else if (action.type === 'change_name') {
    return {...state, name: action.payload}
  }
  return state
}

const initVal = {
  name: '小明',
  age: 22,
  sex: '男',
  hobby: ['唱', '跳']
}

const App = () => {
  const [count, setCount] = useState(0)
  const [state, dispatch] = useReducer(reducer, initVal)

  console.log(state)

  return (
    <div>
      <h1>App</h1>
      <p>姓名: {state.name}
        <button onClick={() => {
          dispatch({
            type: 'change_name',
            payload: Math.random()
          })
        }}>修改姓名</button>
      </p>
      <p>年龄: {state.age}</p>
      <button onClick={() => {
        // 修改useReducer的数据需要调用 dispatch 时传入一个action
        // action: 一个有type字段的对象，用来描述本次如何修改state
        // 调用 dispatch 会执行reducer函数，把action传给reducer
        dispatch({
          type: 'change_age'
        })
      }}>age++</button>
      <hr />
      <p>{count}</p>
      <button onClick={() => setCount(count + 1)}>+</button>
    </div>
  )
}

export default App