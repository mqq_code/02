import React, { useEffect, useState } from 'react'
import { Swiper, SwiperSlide } from 'swiper/react'
import { Navigation, Pagination, Scrollbar, Autoplay } from 'swiper/modules';
import 'swiper/css'
import 'swiper/css/navigation'
import 'swiper/css/pagination'
import 'swiper/css/scrollbar'
import 'swiper/css/autoplay'
import style from './App.module.scss'

const App = () => {
  const [banners, setBanners] = useState([])
  useEffect(() => {
    fetch('https://zyxcl.xyz/music/api/banner')
      .then(res => res.json())
      .then(res => {
        console.log(res.banners)
        setBanners(res.banners)
      })
  }, [])

  return (
    <div>
      <Swiper
        className={style.swiper}
        modules={[Navigation, Pagination, Scrollbar, Autoplay]}
        navigation
        pagination={{ clickable: true }}
        scrollbar={{ draggable: true }}
        autoplay={{
          delay: 1000,
          // disableOnInteraction: true,
          pauseOnMouseEnter: true
        }}
        onSlideChange={() => console.log('slide change')}
        onSwiper={(swiper) => console.log(swiper)}
      >
        {banners.map(item =>
          <SwiperSlide key={item.targetId}>
            <img src={item.imageUrl} alt="" />
          </SwiperSlide>
        )}
      </Swiper>
    </div>
  )
}

export default App