import { Component } from 'react'
import Child from './components/Child'


class App extends Component {

  // // 初始化阶段： 初始化state和props
  // constructor(props) {
  //   super(props)
  //   this.state = {
  //     count: 0
  //   }
  //   console.log('constructor 初始化')
  // }

  // componentDidMount() {
  //   // 组件挂载完成，可以获取到dom元素，可以在此声明周期调用接口
  //   console.log('组件挂载完成 componentDidMount')
  // }

  // componentDidUpdate() {
  //   // 组件渲染成功，可以获取到最新的dom
  //   console.log('componentDidUpdate')
  // }
  state = {
    title: 'aaaa',
    count: 0
  }
  render() {
    const { count, title } = this.state
    console.log('%c App render', 'color: red; font-size: 20px')
    return (
      <div>
        <h2>{title}</h2>
        <div>
          <input type="text" value={title} onChange={e => this.setState({ title: e.target.value })} />
        </div>
        <button onClick={() => {
          this.setState({
            count: this.state.count + 1
          })
        }}>+</button>
        {count}
        <button onClick={() => {
          this.setState({
            count: this.state.count - 1
          })
        }}>-</button>

        {count < 5 && <Child count={count} />}
        <hr />
        {/* {'<b style="color: red">我是一个b标签</b>'} */}
        <div dangerouslySetInnerHTML={{ __html: '<b style="color: red">我是一个b标签</b>' }}></div>
      </div>
    )
  }
}


export default App