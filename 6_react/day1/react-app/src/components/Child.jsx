import React, { Component, PureComponent } from 'react'

// PureComponent: 性能优化，内部使用 shouldComponentUpdate, 比较所有的props和state是否发生改变
export default class Child extends PureComponent {

  // componentWillUnmount() {
  //   // 组件销毁之前，清除异步，例如定时器、事件监听。。。
  //   console.log('Child componentWillUnmount')
  // }

  state = {
    title: '标题'
  }

  // 性能优化，可以判断当前组件中使用的变量是否有更新，减少不必要的更新
  // shouldComponentUpdate(nextProps, nextState) {
  //   // nextProps: 最新的props
  //   // nextState: 最新的state
  //   console.log(nextProps, this.props)
  //   console.log(nextState, this.state)
  //   if (nextState.title !== this.state.title || nextProps.count !== this.props.count) {
  //     return true
  //   }
  //   return false
  // }

  render() {
    console.log('%c Child render', 'color: green; font-size: 20px')

    return (
      <div className='box'>
        <p>父组件的count: {this.props.count}</p>
        <h2>{this.state.title}</h2>
        <button onClick={() => {
          this.setState({
            title: Math.random()
          })
        }}>修改标题</button>
      </div>
    )
  }
}
