import { Component, createRef } from 'react'

class App extends Component {

  state = {
    count: 0,
    title: '标题'
  }
  add = () => {
    // 调用setState修改数据，会触发页面更新
    // setState是异步修改数据的函数
    // 会把传入的对象和当前state种的对象进行合并
    // 连续调用多次 setState react会进行合并更新，只渲染一次组件
    // this.setState({ count: this.state.count + 1 })
    // this.setState({ count: this.state.count + 1 })
    // this.setState({ count: this.state.count + 1 })

    this.setState(prevState => {
      // prevState: 当前最新的state
      // 如果setState传入一个函数作为参数，会把函数return的对象和当前state进行合并
      // console.log(prevState)
      return {
        count: prevState.count + 1
      }
    })
    this.setState(prevState => {
      return {
        count: prevState.count + 1
      }
    })
    this.setState(prevState => {
      return {
        count: prevState.count + 1
      }
    }, () => {
      // 等待本次页面渲染完成执行此函数
      // console.log(this.box.outerHTML)
      console.log(this.boxRef.current)
    })


    // console.log(this.state.count)
  }
  // 创建ref对象
  boxRef = createRef()

  render() {
    // console.log('最新数据', this.state.count)
    return (
      <div>
        <h1>{this.state.title}</h1>
        <button onClick={this.add}>+</button>
        {/* <p ref={el => this.box = el}>{this.state.count}</p> */}
        <p ref={this.boxRef}>{this.state.count}</p>
        <button>-</button>
      </div>
    )
  }
}

export default App