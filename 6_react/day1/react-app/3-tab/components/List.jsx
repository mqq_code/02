import { Component } from 'react'

class List extends Component {
  render() {
    // console.log(this.props.songs)
    return (
      <ul>
        {this.props.songs.map(it =>
          <li key={it}>{it}</li>
        )}
      </ul>
    )
  }
}

export default List