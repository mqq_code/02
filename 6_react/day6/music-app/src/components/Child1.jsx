import React, { useEffect, useState, useRef } from 'react'
import { useCountDown } from '../hooks/countDown'


const Child1 = () => {

  const { num, start, stop, reset } = useCountDown(10)

  return (
    <div className='box'>
      <h2>Child1</h2>
      <p>倒计时: {num}</p>
      <button onClick={start}>开始</button>
      <button onClick={stop}>停止</button>
      <button onClick={reset}>重置</button>
    </div>
  )
}

export default Child1