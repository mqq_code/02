import React, { useEffect, useState, useRef } from 'react'
import { useCountDown } from '../hooks/countDown'
import { useUnmount } from '../hooks/useUnmount'
const Child2 = () => {
  const { num, start } = useCountDown(3000)


  useEffect(() => {
    start()
  }, [])

  const format = s => {
    const h = Math.floor(s / 3600)
    const m = Math.floor(s % 3600 / 60)
    s = s % 60
    return `${h}:${m}:${s}`
  }

  useUnmount(() => {
    console.log('Child2销毁')
  })

  // const forceUpdate = useForceUpdate()






  return (
    <div className='box'>
      <h1>Child2</h1>
      <p>限时秒杀: {format(num)}</p>
      {/* <button onClick={forceUpdate}>强制更新</button> */}
    </div>
  )
}

export default Child2