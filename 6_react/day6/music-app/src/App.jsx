import React, { useState } from 'react'
import Child1 from './components/Child1'
import Child2 from './components/Child2'

// 自定义hook: 抽取组件中的公用逻辑
const App = () => {
  const [title, setTitle] = useState('title')
  const [show, setShow] = useState(true)

  return (
    <div>
      <h1>{title}</h1>
      <button onClick={() => setTitle(Math.random())}>修改标题</button>
      <button onClick={() => setShow(!show)}>显示隐藏</button>
      {show && <Child1 />}
      {show && <Child2 />}
    </div>
  )
}

export default App