import { useState, useEffect, useRef } from 'react'


// 自定义hook的名字必须以 use 开头
export const useCountDown = (n) => {
  const [num, setNum] = useState(n)
  const timer = useRef()

  const start = () => {
    timer.current = setInterval(() => {
      setNum(n => n - 1)
      console.log('定时器')
    }, 1000)
  }

  const reset = () => {
    setNum(10)
    stop()
  }

  const stop = () => {
    clearInterval(timer.current)
  }

  useEffect(() => {
    if (num === 0) {
      clearInterval(timer.current)
    }
  }, [num])

  useEffect(() => {
    return () => {
      console.log('组件销毁')
      stop()
    }
  }, [])


  return {
    num,
    start,
    stop,
    reset
  }
}