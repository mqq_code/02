import React, { useState, useEffect, useRef } from 'react'
import style from './App.module.scss'
import Controller from './components/controller/Controller'
import Keyboard from './components/keyboard/Keyboard'

const App = () => {
  const [list, setList] = useState([])
  const [power, setPower] = useState(true)
  const [bank, setBank] = useState(false)
  const [title, setTitle] = useState('')
  const [volume, setVolume] = useState(90)
  const audioRef = useRef()

  const getList = async () => {
    const res = await fetch('http://zyxcl.xyz/exam_api/music/list').then(res => res.json())
    console.log(res.value)
    setList(res.value)
  }
  useEffect(() => {
    getList()
  }, [])

  useEffect(() => {
    if (!power) {
      setTitle('')
    }
  }, [power])

  const mouseDown = (item, index) => {
    const newList = [...list]
    newList[index].isDown = true
    setList(newList)
    if (power) {
      setTitle(item.id)
      // 放音乐
      const url = bank ? item.bankUrl : item.url
      audioRef.current.src = url
      audioRef.current.play()
    }
  }
  const mouseUp = (item, index) => {
    const newList = [...list]
    newList[index].isDown = false
    setList(newList)
  }

  useEffect(() => {
    audioRef.current.volume = volume / 100
  }, [volume])

  return (
    <div className={style.app}>
      <Keyboard
        list={list}
        power={power}
        onMouseDown={mouseDown}
        onMouseUp={mouseUp}
      />
      <Controller
        title={title}
        power={power}
        bank={bank}
        volume={volume}
        changePower={setPower}
        changeBank={setBank}
        changeVolume={setVolume}
      />
      <audio ref={audioRef} src=""></audio>
    </div>
  )
}

export default App