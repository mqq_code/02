import React, { useEffect, useRef, useState } from 'react'
import style from './Bar.module.scss'

const Switch = (props) => {
  const isDown = useRef(false)
  const barRef = useRef(null)
  const [left, setLeft] = useState(0)

  const mousedown = () => {
    isDown.current = true
  }

  useEffect(() => {
    const maxLeft = barRef.current.getBoundingClientRect().width - 10
    setLeft(maxLeft * (props.value / 100))
  }, [props.value])

  useEffect(() => {
    const barLeft = barRef.current.getBoundingClientRect().left
    const maxLeft = barRef.current.getBoundingClientRect().width - 10

    setLeft(maxLeft * (props.value / 100))

    const mousemove = e => {
      if (isDown.current) {
        let i = e.pageX - barLeft
        if (i < 0) {
          i = 0
        } else if (i > maxLeft) {
          i = maxLeft
        }
        // 通知父组件
        props.onChange(Math.round(i / maxLeft * 100))
        setLeft(i)
      }
    }
    const mouseup = () => {
      isDown.current = false
    }
    document.addEventListener('mousemove', mousemove)
    document.addEventListener('mouseup', mouseup)

    return () => {
      document.removeEventListener('mousemove', mousemove)
      document.removeEventListener('mouseup', mouseup)
    }
  }, [])


  return (
    <div className={style.bar} ref={barRef}>
      <i onMouseDown={mousedown} style={{ left }}></i>
      <p style={{ width: left }}></p>
    </div>
  )
}

export default Switch