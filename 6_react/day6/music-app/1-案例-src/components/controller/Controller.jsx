import React from 'react'
import style from './Controller.module.scss'
import Switch from '../switch/Switch'
import Bar from '../bar/Bar'

const Controller = (props) => {
  return (
    <div className={style.controller}>
      <Switch value={props.power} onChange={props.changePower}>power</Switch>
      <h3>{props.title}</h3>
      <div className={style.bar}>
        <Bar value={props.volume} onChange={props.changeVolume} />
      </div>
      <Switch value={props.bank} onChange={props.changeBank}>bank</Switch>
    </div>
  )
}

export default Controller