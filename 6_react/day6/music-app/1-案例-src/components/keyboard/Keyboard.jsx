import React, { useEffect, useState } from 'react'
import style from './Keyboard.module.scss'
import classNames from 'classnames'

const Keyboard = (props) => {
  
  useEffect(() => {
    const keydown = e => {
      const index = props.list.findIndex(v => v.keyCode === e.keyCode)
      if (index > -1) {
        props.onMouseDown(props.list[index], index)
      }
    }
    const keyup = e => {
      const index = props.list.findIndex(v => v.keyCode === e.keyCode)
      if (index > -1) {
        props.onMouseUp(props.list[index], index)
      }
    }
    document.addEventListener('keydown', keydown)
    document.addEventListener('keyup', keyup)
    return () => {
      document.removeEventListener('keydown', keydown)
      document.removeEventListener('keyup', keyup)
    }
  }, [props.list])

  return (
    <div className={style.keyboard}>
      <ul>
        {props.list.map((item, index) => 
           <li
            key={item.id}
            className={classNames({ [style.active]: item.isDown, [style.orange]: item.isDown && props.power })}
            onMouseDown={() => props.onMouseDown(item, index)}
            onMouseUp={() => props.onMouseUp(item, index)}
          >{item.keyTrigger}</li>
        )}
      </ul>
    </div>
  )
}

export default Keyboard