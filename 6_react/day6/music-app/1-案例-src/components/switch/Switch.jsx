import React from 'react'
import style from './Switch.module.scss'
import classNames from 'classnames'
const Switch = (props) => {
  return (
    <div className={style.switch}>
      <b>{props.children}</b>
      <p
        className={classNames({ [style.on]: props.value })}
        onClick={() => props.onChange(!props.value)}
      >
        <span></span>
      </p>
    </div>
  )
}

export default Switch