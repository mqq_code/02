const express = require('express')

// 创建服务器应用
const app = express()
// app.all('*', (req, res, next) => {
//   res.setHeader('Access-Control-Allow-Origin', '*')
//   next()
// })

app.get('/list/jsonp', (request, response) => {
  // 接收前端传的参数
  const { callbackName } = request.query
  response.setHeader('content-type', 'application/javascript;charset=utf-8')
  response.end(`${callbackName}({name: 1111, age: 2222})`)
})

app.get('/list/jsonp2', (request, response) => {
  const { callbackName } = request.query
  response.setHeader('content-type', 'application/javascript;charset=utf-8')
  response.end(`${callbackName}([1,2,3,4,5])`)
})


app.post('/list/123', (request, response) => {
  console.log(request.host)
  response.send(['我是10086', 1,2,3,4,5,6])
})

// 监听端口
app.listen(10086, () => {
  console.log('running http://localhost:10086')
  console.log('running http://127.0.0.1:10086')
  console.log('running http://10.37.15.96:10086')
})
