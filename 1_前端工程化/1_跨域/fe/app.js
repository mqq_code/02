const express = require('express')
const path = require('path')
// const axios = require('axios')
const { createProxyMiddleware } = require('http-proxy-middleware')

// 创建服务器应用
const app = express()
// 处理静态资源
app.use(express.static(path.join(__dirname, 'dist')))

// 配置前端代理，所有以 /api 开头的请求都自动向转发 target 转发
app.use('/api',
  createProxyMiddleware({
    target: 'http://10.37.15.96:10086',
    changeOrigin: true, // 修改请求 host
  })
)

// 前端代理
// app.get('/123', (req, res) => {
//   axios.post('http://10.37.15.96:10086/list/123').then(response => {
//     res.send(response.data)
//   })
// })


// 监听端口
app.listen(9001, () => {
  console.log('running http://localhost:9001')
  console.log('running http://127.0.0.1:9001')
  console.log('running http://10.37.15.96:9001')
})
