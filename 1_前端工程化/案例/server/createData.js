const Mock = require('mockjs')
const fs = require('fs')
const path = require('path')

const data = Mock.mock({
  "list|200": [{
    "id": "@id",
    "no|+1": 1,
    "name": "@cname",
    "age|18-40": 18,
    "sex|0-1": 0,
    "email": "@email",
    "address": "@county(true)"
  }]
})


fs.writeFile(path.join(__dirname, './user.json'), JSON.stringify(data.list), err => {
  if (err) {
    console.error(err)
  } else {
    console.log('数据创建成功')
  }
})