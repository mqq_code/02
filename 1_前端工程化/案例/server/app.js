const express = require('express')
const ip = require('ip')
const path = require('path')
const fs = require('fs')

const app = express()
app.use(express.json())

const userUrl = path.join(__dirname, './user.json')

const readUser = () => {
  return new Promise((resolve, reject) => {
    fs.readFile(userUrl, 'utf-8', (err, data) => {
      if (err) {
        reject(err)
      } else {
        resolve(JSON.parse(data))
      }
    })
  })
}

const writeUser = (data) => {
  return new Promise((resolve, reject) => {
    fs.writeFile(userUrl, JSON.stringify(data), err => {
      if (err) {
        reject(err)
      } else {
        resolve()
      }
    })
  })
}

// 查询
app.get('/user/list', async (req, res) => {
  try {
    const { page, pagesize, ...other } = req.query
    // 读取所有数据
    const allList = await readUser()
    // 根据传入的筛选条件，从所有数据中查找数据
    const filterList = allList.filter(item => {
      return Object.keys(other).every(key => {
        if (typeof item[key] === 'string') {
          return item[key].includes(other[key])
        } else if (typeof item[key] === 'number') {
          return item[key] === Number(other[key])
        } else {
          return true
        }
      })
    })
    // 截取分页数据
    const current = filterList.slice((page - 1) * pagesize, page * pagesize)
    res.send({
      code: 0,
      msg: '成功',
      total: filterList.length,
      values: current
    })
  } catch(e) {
    console.log('/user/list 接口失败', e)
    res.send({
      code: -2,
      msg: '服务错误',
    })
  }
})

// 添加
app.post('/user/create', async (req, res) => {
  try {
    const userlist = await readUser()
    userlist.push({
      id: Date.now() + '',
      ...req.body
    })
    await writeUser(userlist)
    res.send({
      code: 0,
      msg: '新增成功'
    })
  } catch(e) {
    res.send({
      code: -2,
      msg: '服务错误'
    })
  }
})

// 删除
app.post('/user/del', async (req, res) => {
  try {
    const { id} = req.body
    const userlist = await readUser()
    const index = userlist.findIndex(v => v.id === id)
    if (index > -1) {
      userlist.splice(index, 1)
      await writeUser(userlist)
      res.send({
        code: 0,
        msg: '删除成功'
      })
    } else {
      res.send({
        code: -1,
        msg: '参数错误'
      })
    }
  } catch(e) {
    res.send({
      code: -2,
      msg: '服务错误'
    })
  }
})

// 编辑
app.post('/user/update', async (req, res) => {
  try {
    const {id } = req.body
    const userlist = await readUser()
    const index = userlist.findIndex(v => v.id === id)
    if (index > -1) {
      userlist.splice(index, 1, req.body)
      await writeUser(userlist)
      res.send({
        code: 0,
        msg: '保存成功'
      })
    } else {
      res.send({
        code: -1,
        msg: '参数错误'
      })
    }
  } catch(e) {
    res.send({
      code: -2,
      msg: '服务错误'
    })
  }
})




const PORT = 8001
app.listen(PORT, () => {
  console.log(`服务启动成功 http://localhost:${PORT}`)
  console.log(`服务启动成功 http://127.0.0.1:${PORT}`)
  console.log(`服务启动成功 http://${ip.address()}:${PORT}`)
})

