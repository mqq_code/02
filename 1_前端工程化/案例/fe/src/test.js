
// function ajax(callback, wait) {
//   setTimeout(() => {
//     const res = Math.random()
//     callback(res)
//   }, wait)
// }

// 回调函数处理异步，会有回调地狱的问题
// ajax(res => {
//   console.log(1, res)
//   ajax(res2 => {
//     console.log(2, res2)
//     ajax(res3 => {
//       console.log(3, res3)
//       ajax(res4 => {
//         console.log(4, res4)
//       }, 1000)
//     }, 1000)
//   }, 1000)
// }, 1000)


// function ajax(wait) {
//   return new Promise((resolve, reject) => {
//     setTimeout(() => {
//       const res = Math.random()
//       resolve(res)
//     }, wait)
//   })
// }

// ajax(1000)
//   .then(res1 => {
//     console.log(res1)
//     return ajax(1000)
//   })
//   .then(res2 => {
//     console.log(res2)
//     return ajax(1000)
//   })
//   .then(res3 => {
//     console.log(res3)
//     return ajax(1000)
//   })
//   .then(res4 => {
//     console.log(res4)
//   })

// Promise 是处理异步的解决方案
// 1. 有三种状态，pending、fulfilled、rejected
// 2. 状态只能改变一次
// 3. 调用 resolve(), 状态 pending -> fulfilled，执行 .then 的第一个参数
// 4. 调用 reject(), 状态 pending -> rejected，执行 .then 的第二个参数
// 5. .catch 兜底逻辑，处理报错
// 6. then 会返回一个新的 Promise 对象，可以链式调用

new Promise((resolve, reject) => {
  // console.log(1)
  resolve()
  reject()
})
.then(() => {
  console.log('then')
}, err => {
  console.log('err')
})
.catch(e => {
  console.log('catch')
})
.finally(() => {
  console.log('finally')
})

// console.log(2)

// Promise.all
// Promise.race
// Promise.allSettled
// Promise.resolve
// Promise.reject