import './scss/style.scss'
import axios from 'axios'
import { $, $all } from '@/utils'
import './test'

let page = 1
let pagesize = 5
let totalPage = 0
let other = {} // 搜索表单数据
let currentList = [] // 当前table的数据

// 渲染table
function renderTable(list) {
  $('tbody').innerHTML = list.map((item, index) => {
    return `
      <tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.age}</td>
        <td>${item.sex === 1 ? '男' : '女'}</td>
        <td>${item.email}</td>
        <td>${item.address}</td>
        <td><button data-index="${index}" class="action">操作</button></td>
      </tr>
    `
  }).join('')
}
// 渲染页码
function renderPageBtn(total) {
  totalPage = Math.ceil(total / pagesize)
  // 渲染按钮
  $('.btns').innerHTML = new Array(totalPage).fill(0).map((v, i) => {
    return `<button data-page="${i + 1}" class="${i + 1 === page ? 'active' : ''}">${i + 1}</button>`
  }).join('')
  $('.total').innerHTML = `共 ${total} 条`
  $('.range').innerHTML = `${(page - 1) * pagesize + 1} - ${page * pagesize} 条`
}
// 点击操作按钮，渲染当前行
function renderRow(row, index) {
  return `
    <td>${row.id}</td>
    <td><input class="edit" data-key="name" type="text" value="${row.name}"></td>
    <td><input class="edit" data-key="age" type="text" value="${row.age}"></td>
    <td>
      <select class="edit" data-key="sex">
        <option ${row.sex === 1 ? 'selected' : ''} value="1">男</option>
        <option ${row.sex === 0 ? 'selected' : ''} value="0">女</option>
      </select>
    </td>
    <td><input class="edit" data-key="email" type="text" value="${row.email}"></td>
    <td><input class="edit" data-key="address" type="text" value="${row.address}"></td>
    <td>
      <button class="save" data-id="${row.id}" data-index="${index}">保存</button>
      <button class="del" data-id="${row.id}">删除</button>
      <button class="cancel">取消</button>
    </td>
  `
}

// 绑定事件
function bindEvent() {
  // 页码事件
  $('.btns').addEventListener('click', e => {
    if (e.target.nodeName === 'BUTTON') {
      page = Number(e.target.getAttribute('data-page'))
      getListApi()
    }
  })
  $('.prev').addEventListener('click', () => {
    page--
    if (page < 1) {
      page = 1
      return
    }
    getListApi()
  })
  $('.next').addEventListener('click', () => {
    page++
    if (page > totalPage) {
      page = totalPage
      return 
    }
    getListApi()
  })
  // 筛选事件
  $('.search').addEventListener('click', () => {
    const form = {
      name: $('.name').value.trim(),
      age: $('.age').value.trim(),
      sex: $('.sex').value.trim(),
      address: $('.address').value.trim(),
      email: $('.email').value.trim(),
    }
    Object.keys(form).forEach(key => {
      if (form[key]) {
        other[key] = form[key]
      }
    })
    getListApi()
  })
  $('.reset').addEventListener('click', () => {
    other = {}
    $('.name').value = ''
    $('.age').value = ''
    $('.sex').value = ''
    $('.email').value = ''
    $('.address').value = ''
    getListApi()
  })
  // 操作
  $('body').addEventListener('click', e => {
    if (e.target.classList.contains('action')) {
      const index = e.target.getAttribute('data-index')
      // 存当前行原本内容
      let prevInner = $all('tbody tr')[index].innerHTML
      // 重新渲染当前行内容
      $all('tbody tr')[index].innerHTML = renderRow(currentList[index], index)
      // 点击取消恢复
      $('.cancel', $all('tbody tr')[index]).addEventListener('click', () => {
        $all('tbody tr')[index].innerHTML = prevInner
      })
    } else if (e.target.classList.contains('del')) {
      if (window.confirm('确定要删除此行数据吗？')) {
        const id = e.target.getAttribute('data-id')
        delApi(id)
      }
    } else if (e.target.classList.contains('save')) {
      clickSave(e.target)
    }
  })
  // 新增
  $('.create-btn').addEventListener('click', () => {
    $('.dialog').classList.add('show')
  })
  // 弹窗
  $('.dialog').addEventListener('click', e => {
    // e.target: 触发事件的元素
    // e.currentTarget: 绑定事件的元素
    if (e.target === e.currentTarget || e.target.classList.contains('cancel-btn')) {
      $('.dialog').classList.remove('show')
      clearCreateForm()
    } else if (e.target.classList.contains('confirm')) {
      clickConfirm()
    }
  })
}
// 清空新增输入框的值
function clearCreateForm() {
   $all('.create-inp', $('.dialog-content')).forEach(inp => {
     inp.value = ''
   })
}
// 点击确定新增按钮
function clickConfirm() {
  // 获取弹窗内的所有输入框
  const inps = $all('.create-inp', $('.dialog-content'))
  // 存新增数据
  const params = {}
  // 遍历每一个输入框，把数据存到rowData
  inps.forEach(inp => {
    const key = inp.getAttribute('data-key')
    params[key] = inp.value.trim()
  })
  // 判断非空
  if (hasEmpty(params)) {
    alert('输入框不能为空')
    return
  }
  createApi(params)
}
// 点击保存按钮
function clickSave(saveBtn) {
  const index = saveBtn.getAttribute('data-index')
  const id = saveBtn.getAttribute('data-id')
  // 获取点击的当前行
  const currentRow = $all('tbody tr')[index]
  // 获取当前行内的所有输入框
  const inps = $all('.edit', currentRow)
  // 存当前行数据
  const rowData = {
    id
  }
  // 遍历每一个输入框，把数据存到rowData
  inps.forEach(inp => {
    const key = inp.getAttribute('data-key')
    rowData[key] = inp.value.trim()
  })
  // 判断非空
  if (hasEmpty(rowData)) {
    alert('输入框不能为空')
    return
  }
  // 调用保存接口
  updateApi(rowData)
}

// 获取当前分页数据
async function getListApi() {
  const res = await axios.get('/api/user/list', {
    params: {
      page,
      pagesize,
      ...other
    }
  })
  renderTable(res.data.values)
  renderPageBtn(res.data.total)
  currentList = res.data.values
}
// 删除
async function delApi(id) {
  const res = await axios.post('/api/user/del', { id })
  if (res.data.code === 0) {
    getListApi()
    alert('删除成功')
  } else {
    alert(res.data.msg)
  }
}
// 保存
async function updateApi(params) {
  const res = await axios.post('/api/user/update', params)
  if (res.data.code === 0) {
    getListApi()
    alert('保存成功')
  } else {
    alert(res.data.msg)
  }
}
// 新增
async function createApi(params) {
  const res = await axios.post('/api/user/create', params)
  if (res.data.code === 0) {
    getListApi()
    alert('新增成功')
    $('.dialog').classList.remove('show')
    clearCreateForm()
  } else {
    alert(res.data.msg)
  }
}

// 判断对象是否有空数据
function hasEmpty(obj) {
  return Object.values(obj).some(v => v === '')
}

// 获取列表数据
getListApi()
// 绑定事件
bindEvent()








