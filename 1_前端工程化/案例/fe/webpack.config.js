const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")

// webpack 配置项
module.exports = {
  mode: 'production',
  devtool: 'source-map', // 添加源代码和打包后代码的映射关系
  // 多入口，多出口
  entry: {
    app: './src/index.js'
  },
  output: {
    path: path.join(__dirname, 'build'), // 输出的文件夹，绝对路径
    // 输出的文件名
    // [name] 根据入口配置的key输出对应的文件名
    // [hash:8] 根据文件内容生成哈希值，防止代码更新时用户读缓存
    filename: 'js/[name].[hash:8].js',
    clean: true, // 自动删除之前打包的文件
    assetModuleFilename: 'imgs/[name].[hash:8][ext]' // 图片名称
  },
  module: {
    // 配置各种 loader，解析除 js 和 json 以外的文件
    rules: [
      {
        test: /\.(s?css|sass)$/,
        // MiniCssExtractPlugin.loader 抽离单独的css文件
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|jpe?g|webp|svg|gif)$/,
        type: 'asset/resource', // 使用 webpack5 内置的资源管理模块处理图片
        parser: {
          dataUrlCondition: {
            maxSize: 15 * 1024 // 4kb
          }
        }
      },
      {
        test: /\.html$/,
        use: 'html-loader'
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      }
    ]
  },
  plugins: [ // 配置插件
    new HtmlWebpackPlugin({ // 复制 html 文件，自动把生成的 js 引入到 html
      template: './src/index.html',
      filename: 'index.html',
      chunks: ['app'] // 设置引入的js文件
    }),
    // 配置抽离css文件
    new MiniCssExtractPlugin({
      filename: 'css/[name].[hash:8].css'
    })
  ],
  devServer: { // 配置本地开发服务器
    port: 9002,
    open: true,
    proxy: [
      {
        // 当请求 /api/abc 转发到 http://10.37.15.96:8001/abc
        context: ['/api'],
        target: 'http://10.37.15.96:8001',
        pathRewrite: { '^/api': '' },
        changeOrigin: true,
      }
    ]
  },
  resolve: {
    alias: {
      // 配置常用目录别名
      '@': path.join(__dirname, 'src')
    }
  }
}