> 构建工具：grunt、gulp、webpack、vite
- es6+ => es5
- scss、sass、less => css
- 压缩合并文件：css、html、js
- 压缩图片、转译小图

## webpack
> 作用：根据入口文件开始查找所有依赖项，找到所有依赖项后打包成一个或者多个 js 文件
1. 修改 webpack 默认配置项需要添加配置文件：webpack.config.js


### 配置
1. npm install webpack webpack-cli --save-dev
2. npm install --save-dev html-webpack-plugin
3. npm install --save-dev webpack-dev-server
4. npm i sass sass-loader css-loader style-loader -D
5. npm install -D babel-loader @babel/core @babel/preset-env
6. npm i html-loader 
7. npm install --save-dev mini-css-extract-plugin

### esmodule
1. 抛出
```js
// 单独抛出，必须跟一个声明变量的语句
export let a = 'AAAAA'
export const b = [1,2,3,4]
export function fn() {
}
export const aa = () => {}

// 默认抛出
export default '默认值'

```
2. 导入
```js
// 导入默认值，变量名随意写
import xx from './utils/test'

// 引入单独抛出的变量，变量名必须和抛出的变量一致
import { a, b, fn, aa } from './utils/test'

// 默认值和单独抛出的值一起使用
import de, { a, b } from './utils/test'

// 修改导入的变量名
import { a as a1, b } from './utils/test'

// 导入次文件中所有的变量
import * as all from './utils/test'
```