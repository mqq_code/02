// 导入默认值，变量名随意写
// import xx from './utils/test'

// 引入单独抛出的变量，变量名必须和抛出的变量一致
// import { a, b, fn, aa } from './utils/test'

// 默认值和单独抛出的值一起使用
// import de, { a, b } from './utils/test'

// 修改导入的变量名
// import { a as a1, b } from './utils/test'

// 导入次文件中所有的变量
import * as all from './utils/test'

console.log(all)


const a = 100



// axios.get('/api/sell/api/seller').then(res => {
//   console.log(111, res.data)
// })


// CommonJS 规范
// const xx = require(xxxx) 引入
// module.exports = xxxx 抛出

// es module 规范es6的引入和抛出