> 构建工具：grunt、gulp、webpack、vite
- es6+ => es5
- scss、sass、less => css
- 压缩合并文件：css、html、js
- 压缩图片、转译小图

## webpack
> 作用：根据入口文件开始查找所有依赖项，找到所有依赖项后打包成一个或者多个 js 文件
1. 修改 webpack 默认配置项需要添加配置文件：webpack.config.js


### 配置
1. npm install webpack webpack-cli --save-dev
2. npm install --save-dev html-webpack-plugin
3. npm install --save-dev webpack-dev-server
4. npm i sass sass-loader css-loader style-loader -D
5. npm install -D babel-loader @babel/core @babel/preset-env
6. npm i html-loader 