const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin');

// webpack 配置项
module.exports = {
  mode: 'production',
  entry: './src/index.js', // 入口文件
  output: {
    path: path.join(__dirname, 'build'), // 输出的文件夹，绝对路径
    filename: 'index.js', // 输出的文件名
    clean: true // 自动删除之前打包的文件
  },
  module: {
    // 配置各种 loader，解析除 js 和 json 以外的文件
    rules: [
      {
        test: /\.(s?css|sass)$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.(png|jpe?g|webp|svg|gif)$/,
        type: 'asset', // 使用 webpack5 内置的资源管理模块处理图片
        parser: {
          dataUrlCondition: {
            maxSize: 15 * 1024 // 15kb
          }
        }
      },
      {
        test: /\.html$/,
        use: 'html-loader'
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: 'babel-loader'
      }
    ]
  },
  plugins: [ // 配置插件
    new HtmlWebpackPlugin({ // 复制 html 文件，自动把生成的 js 引入到 html
      template: './src/index.html',
      filename: 'index.html'
    })
  ],
  devServer: { // 配置本地开发服务器
    port: 9002,
    open: true
  }
}