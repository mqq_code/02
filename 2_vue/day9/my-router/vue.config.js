const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    proxy: {
      '/zyx': {
        target: 'https://api.map.baidu.com',
        pathRewrite: { '^/zyx': '' }
      }
    }
  }
})
