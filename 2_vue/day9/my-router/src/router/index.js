import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/home/Home.vue'
import Detail from '../pages/detail/Detail.vue'
import Login from '../pages/login/Login.vue'
import Search from '../pages/search/Search.vue'
import Notfound from '../pages/404.vue'
import Movie from '../pages/home/movie/Movie.vue'
import Cinema from '../pages/home/cinema/Cinema.vue'
// import Mine from '../pages/home/mine/Mine.vue'
import MovieList from '../pages/home/movie/movieList/MovieList.vue'

// 给 vue 添加路由功能
Vue.use(VueRouter)

// vue-router重写push方法，解决相同路径跳转报错
const routerPush = VueRouter.prototype.push
VueRouter.prototype.push = function push (location) {
  // 这个语句用来解决报错
  // 调用原来的push函数，并捕获异常
  return routerPush.call(this, location).catch(err => err)
}

// 创建路由实例对象
const router = new VueRouter({
  // mode: 'hash', // url后边有 #
  mode: 'history', // url没有 #
  // 配置路由，指定地址需要展示的组件
  routes: [
    {
      // 当地址 === / 自动跳转到 /home
      path: '/',
      redirect: '/home' // 重定向
    },
    {
      // 当url展示/home的时候，页面自动渲染Home组件
      path: '/home',
      name: 'home',
      component: Home,
      redirect: '/home/movie',
      meta: { title: '首页' },
      children: [
        {
          path: '/home/movie',
          name: 'movie',
          component: Movie,
          meta: { title: '电影' },
          redirect: '/home/movie/hot',
          children: [
            {
              path: '/home/movie/:type',
              name: 'movielist',
              component: MovieList,
              meta: { title: '电影' }
            }
          ]
        },
        {
          path: '/home/cinema',
          name: 'cinema',
          component: Cinema,
          meta: { title: '影院' }
        },
        {
          path: '/home/mine',
          name: 'mine',
          meta: { title: '个人中心' },
          beforeEnter: (to, from, next) => {
            const token = localStorage.getItem('token')
            if (!token) {
              next('/login')
              return
            }
            next()
          },
          component: () => import(/* webpackChunkName: 'mine' */'../pages/home/mine/Mine.vue')
        }
      ]
    },
    {
      // 动态路由：/detail/:参数
      path: '/detail/:filmId',
      name: 'detail',
      component: Detail,
      meta: {
        title: '详情',
        needLogin: true
      }
    },
    {
      path: '/search',
      name: 'search',
      component: Search,
      meta: {
        needLogin: true,
        title: '搜索'
      }
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: { title: '登录' }
    },
    {
      path: '*',
      name: '404',
      component: Notfound
    }
  ]
})

// 全局前置守卫：所有路由跳转之前都会先执行此函数
router.beforeEach((to, from, next) => {
  // to: 目标路由
  // from: 来源路由
  if (to.meta.title) {
    document.title = to.meta.title
  }
  if (to.meta.needLogin) {
    const token = localStorage.getItem('token')
    if (!token) {
      // 需要登录并且没有token
      // console.log('原本要去的页面', to)
      // 把原本要去页面url传给登录页
      next({
        path: '/login',
        query: {
          redirect: to.fullPath
        }
      })
      return
    }
  }
  next()
})
// 后置钩子函数，路由跳转完成之后执行
router.afterEach((to, from) => {
  // ...
})

export default router
