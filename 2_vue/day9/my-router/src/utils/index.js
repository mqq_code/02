export const getLocation = () => {
  return new Promise((resolve, reject) => {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition(
        function (position) {
          resolve({
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
          })
        },
        function (error) {
          console.log(error)
          reject(new Error({
            code: error.code,
            message: error.message
          }))
        }
      )
    } else {
      reject(new Error('Geolocation API 不支持。'))
    }
  })
}