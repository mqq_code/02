import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VConsole from 'vconsole'

window.MyConsole = new VConsole()

Vue.config.productionTip = false
Vue.config.devtools = true
Vue.prototype.$bus = new Vue()

document.addEventListener('touchstart', function (event) {
  if (event.scale !== 1) {
    event.preventDefault()
  }
}, false)

new Vue({
  // 把路由实例对象添加到vue实例对象中
  router,
  render: h => h(App)
}).$mount('#app')
