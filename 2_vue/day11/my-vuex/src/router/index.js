import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Detail from '../views/Detail.vue'
import Search from '../views/Search.vue'
import HomeList from '../views/HomeList.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    name: Home,
    component: Home,
    redirect: '/home/zh',
    children: [
      {
        path: '/home/:type',
        name: HomeList,
        component: HomeList
      }
    ]
  },
  {
    path: '/detail',
    name: Detail,
    component: Detail
  },
  {
    path: '/search',
    name: Search,
    component: Search
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
