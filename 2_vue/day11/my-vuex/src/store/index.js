import Vue from 'vue'
import Vuex, { createLogger } from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    isGrid: false,
    sortType: 0 // 0: 默认, 1: 升序, 2:降序
  },
  mutations: {
    changeGrid (state) {
      state.isGrid = !state.isGrid
    },
    changeSort (state) {
      state.sortType++
      if (state.sortType > 2) {
        state.sortType = 0
      }
    }
  },
  plugins: [createLogger()],
  modules: {
  }
})
