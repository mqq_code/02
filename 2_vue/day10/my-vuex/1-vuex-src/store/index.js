import Vue from 'vue'
import Vuex, { createLogger } from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  // 存全局数据，所有的组件都可以直接获取
  state: {
    count: 0,
    list: [],
    banners: []
  },
  // 修改数据的唯一方式，必须是同步函数
  mutations: {
    changeCount (state, payload) {
      state.count += payload
      // 如果在此函数中使用异步，插件或者devtool无法捕获到数据修改
      // setTimeout(() => {
      //   state.count += payload
      // }, 3000)
    },
    pushList (state, payload) {
      state.list.push(payload)
      state.count = 10000
    },
    setBanners (state, banners) {
      state.banners = banners
    }
  },
  // 类似组件中的计算属性，根据一个或者多个变量生成一个新变量，依赖项改变时会自动计算结果，没改变就从缓存度数据
  getters: {
    len (state) {
      return state.count + state.list.length
    }
  },
  // 处理异步
  actions: {
    getBannerApi (context, params) {
      // context: 类似Store实例对象
      axios.get('http://zyxcl.xyz/music_api/banner')
        .then(res => {
          // console.log(context)
          console.log(params)
          context.commit('setBanners', res.data.banners)
        })
        .catch(e => {
          console.log(e)
        })
    }
  },
  // 修改数据时自动打印修改前后的数据对比和时间
  plugins: [createLogger()]
})

// ● Vuex 是一个专为 Vue.js 应用程序开发的状态管理模式，存全局数据。
// ● store 内有 5 个属性，分别是 state、getter、mutation、action、module
// ● state 存放数据，组件内通过 this.$store.state 或者 mapState 辅助函数获取
// ● mutations 是修改 state 数据的唯一方式，必须是同步函数，在组件内通过 this.$store.commit(函数名, 参数) 调用，或者使用 mapMutations 辅助函数映射到组件的 methods 内
// ● actions  处理 store 中的异步操作，然后触发 mutations 中的函数修改 state，在组件中通过 this.$store.dispatch(函数名, 参数) 触发 actions 中的函数，或者通过辅助函数 mapActions 映射到组件的 methods 中
// ● getters 可以对 state 的数据进行计算，类似组件中的 computed