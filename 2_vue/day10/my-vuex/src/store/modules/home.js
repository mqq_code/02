
const homeModule = {
  // 开启命名空间，mutations 和 actions 不会和全局同名
  namespaced: true,
  state: () => ({
    title: '我是home的标题'
  }),
  mutations: {
    changeTitle (state, payload) {
      console.log('我是home的changetitle')
      state.title = payload
    }
  },
  actions: {}
}

export default homeModule