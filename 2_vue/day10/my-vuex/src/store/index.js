import Vue from 'vue'
import Vuex, { createLogger } from 'vuex'
import homeModule from './modules/home'
import aboutModule from './modules/about'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // 根数据
    title: '我是最外层的标题'
  },
  mutations: {
    changeTitle (state, payload) {
      console.log('我是最外层的changetitle')
      state.title = payload
    }
  },
  plugins: [createLogger()],
  modules: {
    // 注册子模块
    homeModule,
    aboutModule
  }
})
