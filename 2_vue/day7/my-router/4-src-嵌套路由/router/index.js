import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../pages/Home.vue'
import Detail from '../pages/Detail.vue'
import Login from '../pages/Login.vue'
import Search from '../pages/Search.vue'
import Notfound from '../pages/404.vue'
import Movie from '../pages/Movie.vue'
import Cinema from '../pages/Cinema.vue'
import Mine from '../pages/Mine.vue'
import MovieList from '../pages/MovieList.vue'

// 给 vue 添加路由功能
Vue.use(VueRouter)

// 创建路由实例对象
const router = new VueRouter({
  // mode: 'hash', // url后边有 #
  mode: 'history', // url没有 #
  // 配置路由，指定地址需要展示的组件
  routes: [
    {
      // 当地址 === / 自动跳转到 /home
      path: '/',
      redirect: '/home' // 重定向
    },
    {
      // 当url展示/home的时候，页面自动渲染Home组件
      path: '/home',
      name: 'home',
      component: Home,
      redirect: '/home/movie',
      children: [
        {
          path: '/home/movie',
          name: 'movie',
          component: Movie,
          redirect: '/home/movie/hot',
          children: [
            {
              path: '/home/movie/:type',
              name: 'movielist',
              component: MovieList
            }
          ]
        },
        {
          path: '/home/cinema',
          name: 'cinema',
          component: Cinema
        },
        {
          path: '/home/mine',
          name: 'mine',
          component: Mine
        }
      ]
    },
    {
      // 动态路由：/detail/:参数
      path: '/detail/:id',
      name: 'detail',
      component: Detail
    },
    {
      path: '/search',
      name: 'search',
      component: Search
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '*',
      name: '404',
      component: Notfound
    }
  ]
})

export default router
