import Vue from 'vue'
import App from './App.vue'
import router from './router'

Vue.config.productionTip = false
Vue.config.devtools = true

new Vue({
  // 把路由实例对象添加到vue实例对象中
  router,
  render: h => h(App)
}).$mount('#app')
