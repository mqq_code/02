import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home/Home.vue'
import Child1 from '../views/home/Child1.vue'
import Child2 from '../views/home/Child2.vue'
import Child3 from '../views/home/Child3.vue'
import Child4 from '../views/home/Child4.vue'
import Child5 from '../views/home/Child5.vue'
import Login from '../views/login/Login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: {
      isLogin: true
    },
    redirect: '/child1',
    children: [
      {
        path: '/child1',
        name: 'child1',
        component: Child1
      },
      {
        path: '/child2',
        name: 'child2',
        component: Child2
      },
      {
        path: '/child3',
        name: 'child3',
        component: Child3
      },
      {
        path: '/child4',
        name: 'child4',
        component: Child4
      },
      {
        path: '/child5',
        name: 'child5',
        component: Child5
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  // 判断当前和父级路由只要有一个需要登录就拦截
  if (to.matched.some(v => v.meta.isLogin)) {
    const token = localStorage.getItem('token')
    if (!token) {
      next('/login')
      return
    }
  }
  next()
})

export default router
