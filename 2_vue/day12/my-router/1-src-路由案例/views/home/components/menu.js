const menuList = [
  {
    title: '资金管理',
    children: [
      { text: '导航1', path: '/child1' },
      { text: '导航2', path: '/child2' },
      { text: '导航3', path: '/child3' }
    ]
  },
  {
    title: '系统管理',
    children: [
      { text: '导航4', path: '/child4' },
      { text: '导航5', path: '/child5' }
    ]
  }
]

export default menuList