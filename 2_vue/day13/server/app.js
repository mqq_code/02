const http = require('http')
const fs = require('fs')
const path = require('path')

const app = http.createServer((request, response) => {

  let curUrl = request.url
  if (curUrl === '/') curUrl = '/index.html'
  console.log('要访问的地址', curUrl)
  const fullPath = path.join(__dirname, 'dist', curUrl)
  console.log('要访问的文件', fullPath)
  if (fs.existsSync(fullPath)) {
    response.end(fs.readFileSync(fullPath, 'utf-8'))
    return
  }

  // response.end('404')

  // 兜底逻辑，处理前端 history 模式路由刷新404的问题
  const indexPath = path.join(__dirname, 'dist/index.html')
  response.end(fs.readFileSync(indexPath, 'utf-8'))
})

app.listen(8001, () => {
  console.log('服务启动成功 http://10.37.15.96:8001')
})