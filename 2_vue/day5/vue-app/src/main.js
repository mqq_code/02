import Vue from 'vue'
import './scss/common.scss'
import App from './App.vue'
import loadingImg from './assets/loading.gif'
import errorImg from './assets/error.webp'

// import Test from './components/Test.vue'

// 一个vue文件就是一个组件，组件包括排版、样式、逻辑
// 组件作用：
// 1. 方便复用，减少重复代码
// 2. 拆分页面，提高代码可维护性

// // 全局组件
// Vue.component('Test', Test)



function loadImg(el, src) {
  // 显示loading
  el.src = loadingImg
  // 开始加载
  const img = new Image()
  img.src = src
  img.onload = function () {
    setTimeout(() => {
      console.log('加载成功')
      el.src = src
    }, 2000)
  }
  img.onerror = function () {
    setTimeout(() => {
      console.log('加载失败')
      el.src = errorImg
    }, 2000)
  }
}

// 注册全局指令
Vue.directive('load', {
  // 指令第一次绑定到元素时调用，可以初始化
  bind(el, binding) {
    console.log('bind -> el', el)
    console.log('bind -> binding', binding)
    loadImg(el, binding.value)
  },
  // 被绑定元素插入父节点时调用，可以获取到父元素
  inserted(el) {
    // el.parentNode.style.border = '2px solid red'
  },
  // 组件数据更新，可能没有更新完成
  update(el, binding) {
    console.log('update -> el', el)
    console.log('update -> binding', binding)
    if (binding.oldValue !== binding.value) {
      loadImg(el, binding.value)
    }
  },
  // 所在组件全部更新完成
  componentUpdated() {},
  // 指令解绑时执行，元素或者组件销毁
  unbind() {
    console.log('unbind')
  }
})

new Vue({
  render: h => h(App),
}).$mount('#app')

