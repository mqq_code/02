const posMixin = {
  data() {
    return {
      pos: {
        x: 0,
        y: 0
      },
      isDown: false,
      abc: '我是混入的abc'
    }
  },
  computed: {},
  watch: {},
  methods: {},
  created() {
    this.mousemove = e => {
      if (this.isDown) {
        this.pos = {
          x: e.pageX,
          y: e.pageY
        }
      }
    }
    this.mousedown = () => {
      this.isDown = true
    }
    this.mouseup = () => {
      this.isDown = false
    }
    document.addEventListener('mousemove', this.mousemove)
    document.addEventListener('mousedown', this.mousedown)
    document.addEventListener('mouseup', this.mouseup)
    console.log('混入的created')
  },
  beforeDestroy() {
    document.removeEventListener('mousemove', this.mousemove)
    document.removeEventListener('mousedown', this.mousedown)
    document.removeEventListener('mouseup', this.mouseup)
  }
}

export default posMixin