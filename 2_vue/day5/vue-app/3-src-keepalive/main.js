import Vue from 'vue'
import './scss/common.scss'
import App from './App.vue'
// 一个vue文件就是一个组件，组件包括排版、样式、逻辑
// 组件作用：
// 1. 方便复用，减少重复代码
// 2. 拆分页面，提高代码可维护性

new Vue({
  render: h => h(App),
}).$mount('#app')

