原生js： 操作dom
vue、react 框架：数据驱动视图，修改数据，dom自动更新

### Vue
1. 渐进式式前端框架
2. 响应式数据，数据驱动视图（修改数据页面更新）
3. 数据双向绑定
4. 组件化开发

### 声明式渲染
- 声明式渲染，只能写表达式，不能写语句
```html
<div class="app">
  <h1>{{ title }}</h1>
  <p>{{ title.split('') }}</p>
</div>
<script>
  const vm = new Vue({
    el: '.app', // 挂载的元素，在此元素内可以使用vue的变量
    data: { // 定义 vue 变量，会自动添加到 vue 实例对象上
      title: '开始学习Vue'
    }
  })
</script>
```

### 指令
- 标签上以 v- 开头的特殊属性
- 可以使用指令完成常用的 dom 操作
1. v-text: 渲染标签内的文本
2. v-html: js中的 innerHTML,可以解析标签
3. v-show: 条件判断，添加删除元素的 display: none;
4. v-if、v-else-if、v-else: 条件判断，添加删除 dom 元素
5. v-for: 遍历array、object、string、number
6. v-on 简写 @ ：绑定事件
- 事件修饰符
- .stop 阻止冒泡
- .prevent 阻止默认行为
- .self 只当事件是从侦听器绑定的元素本身触发时才触发回调
- .capture 事件捕获的方式绑定事件
- .{keyCode | keyAlias} - 只当事件是从特定键触发
- .once 只触发一次事件
```html
<!-- 事件内执行代码 -->
<button v-on:click="num--">-</button>
<!-- 事件执行函数 -->
<button v-on:click="add">+</button>
<!-- 执行函数传参数：$event: 事件对象 -->
<button @click="changeNum(3, $event)">+3</button>
<!-- 执行箭头函数 -->
<input type="text" @input="e => title = e.target.value">
<!-- $event: 事件对象 -->
<input type="text" @input="title = $event.target.value">
```
7. v-bind 简写 : 让标签的属性可以使用 js 变量
8. v-model 表单数据双向绑定
- checkbox： 
  - v-model 绑定的变量是 布尔值，获取元素的 checked 属性
  - v-model 绑定的变量是 数组，获取元素的 value 属性
- 修饰符
  - .trim 去除首尾空格
  - .number 把value转成数字
  - .lazy change事件触发后修改数据


### ref
- 获取dom元素
```html
<div class="app">
  <input ref="inp" type="text" />
  <button @click="submit">按钮</button>
</div>

<script>
  const vm = new Vue({
    el: '.app',
    data: {
    },
    methods: {
      submit() {
        // 获取 input 元素
        console.log(this.$refs.inp.value)
      }
    }
  })
</script>
```


### $set
- 修改数据，更新页面
```js
// 通过下标修改数组
this.$set(this.arr, 下标, 值)
// 给对象新增属性，更新页面
this.$set(this.obj, 属性名, 属性值)
```

### 修改数组和对象
1. 数组
```js
// 通过下标或 length 修改数组，页面不会自动更新
// this.arr[1] = 'ABC'
// this.arr.length = 0

// 调用数组的方法或者 $set 修改数组
this.$set(this.arr, 1, 'ABC')
// this.arr.splice(1, 1, 'ABC')
```
2. 对象
```js
// 新增属性，不会自动更新页面
// this.obj.sex = Math.random()

// 修改原本就存在属性可以更新页面
this.obj.name = Math.random()

// 给整个对象重新赋值或者使用 $set，可以更新页面
this.obj = { ...this.obj, sex: Math.random() }
this.$set(this.obj, 'sex', Math.random())
```

### $nextTick
- 等待页面更新完成之后执行的回调
```js
this.$nextTick(() => {
  // 等待页面更新完成之后执行次函数
})
```

### v-for中的key
- key必须是一个唯一值
- 主要作用是为 Vue 的虚拟 DOM 算法提供一个唯一标识，让 Vue 能够追踪每个节点的身份，从而重用和重新排序现有元素

### 计算属性
- 作用：根据一个或者多个响应式变量生成一个新变量，计算属性会自动监听函数内使用的响应式变量，依赖项更新自动执行函数计算结果，没有更新从缓存中读取结果
```js
computed: {
  fullName() {
    // 当 firstName 和 lastName 改变时自动执行次函数返回结果
    return  `我的名字叫：${this.firstName} ${this.lastName}`
  },
}
```

### watch
- 作用：监听某个变量改变，自动执行函数
```js
watch: {
  // num 改变自动执行次函数
  num(newVal, oldVal) {
    // newVal: 改变后的值
    // olVal: 改变前的值
  },
  // keyword 改变自动执行 methods 中的  startSearch 函数
  keyword: 'startSearch',
  // 监听 obj 地址改变，无法监听到对象的属性改变
  obj() {
    console.log('obj改变了', this.obj)
  },
  // 可以监听 obj 地址或者属性改变
  obj: {
    handler() {
      console.log('obj改变了', this.obj)
    },
    // 深度监听
    deep: true
  }
  // 监听对象的某一个属性改变
  'obj.name': function() {
    console.log('obj.name改变了', this.obj.name)
  },
  // 进入页面立即执行一次函数
  num: {
    handler() {
      console.log('num改变了', this.num)
    },
    // 立即执行函数
    immediate: true
  }
}
```

### 生命周期
- 生命周期：vue实例从创建到销毁的过程
- 生命周期钩子函数: 在特定时机自动执行的函数
1. 创建阶段
  - beforeCreate： 实例创建之前，vue实例还未创建，vue相关的数据和方法不可用，处理和vue无关的逻辑
  - created：实例创建完成，vue的数据、计算属性、方法可以是用，但是页面还没有渲染，可以调用接口、格式化数据、创建定时器...
2. 挂载阶段
  - beforeMount：页面挂载之前，可以修改数据
  - mounted：页面挂载完成，能获取到页面上最新的dom元素
3. 更新阶段
  - beforeUpdate：数据改变后页面更新前，可以获取最新的数据，无法获取最新的dom
  - updated：页面更新完成，可以获取到最新的dom元素
4. 销毁阶段
  - beforeDestroy：销毁之前, 清除页面上的异步任务：定时器、原生事件
  - destroyed：销毁完成


### 使用 @vue/cli
1. `npm install -g @vue/cli`  全局安装 cli 工具
2. `vue -V` 查看版本号
3. `vue create 项目名称` 创建项目

## 组件
- 封装重复代码（排版、样式、功能），方便多次复用
- 拆分代码逻辑，每个组件维护单独的状态，方便理解和后期维护

### 组件注意事项
- 组件的data必须是一个函数，每次调用组件都会执行 data 函数创建新对象，防止组件复用时数据互相影响
- 子组件不能改变父组件传入的数据，组件中的数据必须在定义的组件中修改
- 样式隔离：`<style scoped>` 样式只在当前组件内生效，不影响全局样式，给组件内的标签添加自定义属性，通过属性选择器定义样式实现样式隔离功能


### 组件通讯
1. 父组件向子组件传值：通过 Props 传递数据，子组件通过 props 属性接收
- 子组件
```html
<template>
  <div>使用父组件的变量：{{ title }}</div>
</template>

<script>
export default {
  // 接收父组件传过来的变量
  props: ['title']
}
</script>
```
- 父组件
```html
<template>
  <div>
    <Child :title="title" />
  </div>
</template>

<script>
import Child from './Child.vue'
export default {
  data() {
    return {
      title: '标题'
    }
  },
  components: {
    Child
  }
}
</script>
```
2. 子组件向父组件传值：通过自定义事件进行数据传递，子组件通过调用 $emit 调用父组件的函数
- 子组件
```html
<template>
  <div>
    <button @click="handleClick">按钮</button>
  </div>
</template>

<script>
export default {
  methods: {
    handleClick() {
      // 调用自定义事件
      this.$emit('anyEvents', 参数)
    }
  }
}
</script>
```
- 父组件
```html
<template>
  <div>
    <Child @anyEvents="getChildData" />
  </div>
</template>

<script>
import Child from './Child.vue'
export default {
  methods: {
    getChildData(data) {
      // data: 子组件传过来的数据
    }
  },
  components: {
    Child
  }
}
</script>
```
3. 同级组件或者无关系组件: eventBus（发布订阅模式）
4. 跨级组件传值：provide/inject
- 父级组件通过 procide 向所有后代组件传数据
- 后代组件通过 inject 接收 provide 的数据
- ***注意：*** provide 传递的参数不是响应式的，除非是一个响应式对象

### props 校验和默认值
- 子组件
```html
<script>
export default {
  // 数组方式接收props，无法校验类型，无法设置默认值
  // props: ['title', 'showClose', 'okText', 'cancelText', 'num']
  // 对象形式接收参数，可以校验类型和设置默认值
  props: {
    title: String, // 参数类型
    showClose: {
      type: [Boolean, Number], // 多种类型
      default: true // 默认值
    },
    num: {
      type: Number,
      required: true, // 必填
      default: 0
    },
    test: {
      // 自定义校验
      validator: function (value) {
        return value === 100 || value === '100'
      }
    }
  },
}
</script>
```

### 插槽 slot
> 当组件中的某一部分布局在调用组件时才能确定的情况可以使用插槽占位
1. 默认插槽
- 子组件
```html
<template>
  <div>
    <button @click="handleClick">按钮</button>
    <!-- 父组件没有提供插槽内容，则显示“默认内容”。 -->
    <slot>默认内容</slot>
  </div>
</template>

```
- 父组件
```html
<template>
  <div>
    <Child>
      <!-- 插槽内容，使用此处内容覆盖子组件内的 <slot></slot> -->
      <ul>
        <li>111</li>
      </ul>
    </Child>
  </div>
</template>

```
2. 具名插槽: 子组件内有多个插槽时使用
- 子组件
```html
<template>
  <div>
    <slot name="header"></slot>
    <slot name="footer"></slot>
  </div>
</template>

```
- 父组件
```html
<template>
  <div>
    <Child>
      <template v-slot:header>
        <h1>页面标题</h1>
      </template>
      <template v-slot:footer>
        <p>底部内容</p>
      </template>
    </Child>
  </div>
</template>

```
3. 作用域插槽: 作用域插槽允许子组件将数据传递给插槽，使得父组件可以访问子组件的数据。
- 子组件
```html
<template>
  <div>
    <slot name="header" a="AA" :b="100"></slot>
  </div>
</template>

```
- 父组件
```html
<template>
  <div>
    <Child>
      <template v-slot:item="slotProps">
        <!-- slotProps: { a: 'AA', b: 100 } -->
        <span>{{ slotProps.a }}</span>
      </template>
    </Child>
  </div>
</template>

```
### 组件上的 v-model
- 子组件
```html
<template>
  <input
    :value="value"
    @input="$emit('input', $event.target.value)"
  >
</template>

<script>
export default {
  props: ['value']
}
</script>
```
- 父组件
```html
<template>
  <!-- 完整写法 -->
  <CustomInput :value="inputValue" @input="inputValue = $event" />
  <!-- 简写 -->
  <CustomInput v-model="inputValue" />
</template>

<script>
import CustomInput from './CustomInput.vue';

export default {
  components: { CustomInput },
  data() {
    return {
      inputValue: ''
    }
  }
}
</script>
```
### 组件参数的 .sync 修饰符
- 子组件
```html
<template>
  <input :value="myProp" @input="onInput">
</template>

<script>
export default {
  props: ['myProp'],
  methods: {
    onInput(event) {
      this.$emit('update:myProp', event.target.value);
    }
  }
}
</script>
```
- 父组件
```html
<template>
  <ChildComponent :my-prop.sync="parentProp" />
</template>

<script>
import ChildComponent from './ChildComponent.vue';

export default {
  components: {
    ChildComponent
  },
  data() {
    return {
      parentProp: 'initial value'
    }
  }
}
</script>
```



## Router
- 让单页面实现多页面的功能
- 单页面应用 SPA (single page application)
- 多页面应用 MPA（multiple page application）

### 配置路由
```js
// 创建 router 实例对象
const router = new VueRouter({
  // 路由模式: 
  // hash => url 中显示 #，利用 hashchange 事件监听#后的内容改变，展示对应的组件
  // history => url 没有#，h5 history API 的 pushState replaceState  修改历史记录，上线后需要后端配置
  mode: 'hash',
  // 配置路由页面
  routes: [
    {
      // 地址为 /home 时，页面展示 Home 组件
      path: '/home',
      name: 'home',
      component: Home,
      // 重定向
      redirect: '/home/movie',
      // 配置子路由，此处路由只在home页面中展示
      children: [
        {
          path: '/home/movie',
          name: 'movie',
          component: Movie
        }
      ]
    },
    {
      // 配置动态路由
      path: '/detail/:id',
      name: 'detail',
      component: Detail
    }
  ]
})
```
### 使用路由
1. `<router-view />` 路由在页面渲染的位置
2. `this.$router` 路由实例对象，包含跳转方法
3. `this.$route` 当前路由信息，包含当前地址和参数

### 跳转路由
2. 声明式导航
- `<router-link to="地址">导航</router-link>` 
- `<router-link :to="{ path: '地址' }">导航</router-link>` 
3. 编程式导航
- `this.$router.push()` 
- `this.$router.replace()` 
- `this.$router.go()` 
- `this.$router.back()` 
- `this.$router.forward()` 

### 路由传参数
1. query 传参数
```js
this.$router.push(`/detail?id=${id}&a=100&b=200`)
this.$router.push({
  path: '/detail',
  query: {
    id,
    a: 'AAA',
    b: 'BBB'
  }
})
```
2. 动态路由传参数 ***注意：必须先在路由表配置***
```js
this.$router.push(`/detail/${id}/aaa/bbbb`)
this.$router.push({
  name: 'detail',
  params: {
    id,
    a: 'AAAA',
    b: 'VBVVVV'
  },
  query: {
    test: 'test111',
    user: 'wangxiaoming'
  }
})
```

### 获取路由参数
1. 获取query参数 `this.$route.query`
2. 获取params参数 `this.$route.params`
3. 监听路由参数改变
```js
export default {
  watch: {
    // 监听路由信息改变
    $route() {
      console.log('路由改变了', this.$route)
    }
  }
}
```

### 路由守卫
1. 全局守卫（给所有路由添加拦截）
- `beforeEach((to, from, next) => {})`  路由跳转之前，组件加载之前，一般用来处理登录拦截
- `beforeResolve((to, from, next) => {})`  路由跳转之前，组件解析后
- `afterEach((to, from) => {})` 路由跳转之后，可以统计页面访问信息
2. 路由独享守卫（给某一个路由添加拦截）
- `beforeEnter((to, from, next) => {})` 路由跳转之前
3. 组件内守卫（针对某一个组件添加拦截）
- `beforeRouteEnter` 在渲染该组件的对应路由被确认前调用，不能使用this
- `beforeRouteUpdate` 在当前路由改变，但是该组件被复用时调用，参数改变时调用
- `beforeRouteLeave` 离开该组件的对应路由时调用，可以阻止页面离开
