import Vue from 'vue'
import './scss/common.scss'
import App from './App.vue'
// 一个vue文件就是一个组件，组件包括排版、样式、逻辑
// 组件作用：
// 1. 方便复用，减少重复代码
// 2. 拆分页面，提高代码可维护性

new Vue({
  render: h => h(App),
}).$mount('#app')



// 如果data是一个对象，组件复用时数据可能会互相影响，
// 使用一个函数返回对象，每次组件调用时执行data函数返回新对象，可以防止组件的数据互相影响

// const data = () => {
//   return {
//     num: 0,
//     title: '123'
//   }
// }


// const com1 = data()
// const com2 = data()
// const com3 = data()

// com1.num++

// console.log(com1)
// console.log(com2)
// console.log(com3)







// const com = {
//   data: {
//     num: 0,
//     title: 0
//   },
//   created() {

//   }
// }

// const com1 = {...com}
// const com2 = {...com}
// const com3 = {...com}

// com1.data.num = 10

// console.log(com1)
// console.log(com2)
// console.log(com3)


// 堆和栈
let a = 100
let b = 'abc'
const c = { age: 100 }
const d = [1,2,3,4,5]
const f = function() {}

c.age = 20















