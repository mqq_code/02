import Vue from 'vue'
import 'animate.css'
import './scss/common.scss'
import App from './App.vue'

new Vue({
  render: h => h(App),
}).$mount('#app')

