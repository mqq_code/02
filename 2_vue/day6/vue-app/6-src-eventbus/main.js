import Vue from 'vue'
import 'animate.css'
import './scss/common.scss'
import App from './App.vue'
import event from './event'

// eventBus (发布订阅模式): 处理同级组件或者无关系组件之间的通讯
Vue.prototype.$bus = event

new Vue({
  render: h => h(App),
}).$mount('#app')

