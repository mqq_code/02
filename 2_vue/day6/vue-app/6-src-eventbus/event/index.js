import Vue from 'vue'

// 创建发布订阅对象
const events = new Vue()

export default events



class Event {
  list = {}
  on(key, callback) {
    if (this.list[key]) {
      this.list[key].push(callback)
    } else {
      this.list[key] = [callback]
    }
  }
  emit(key, ...rest) {
    this.list[key].forEach(fn => {
      fn(...rest)
    })
  }
  off(key, callback) {
    const index = this.list[key].indexOf(callback)
    this.list[key].splice(index, 1)
  }
  once(key, callback) {
    const onceFn = (...rest) => {
      callback(...rest)
      this.off(key, onceFn)
    }
    this.on(key, onceFn)
  }
}




// 发布订阅模式: 注册、销毁、调用、once

window.$ev = new Event()

const abc1 =  (a) => {
  console.log(a, '调用了abc11111')
}
window.$ev.on('abc', abc1)


setTimeout(() => {
  window.$ev.off('abc', abc1)
}, 6000)


window.$ev.on('abc', (a) => {
  console.log(a, '调用了abc22222')
})


window.$ev.once('add', (a, b, c) => {
  console.log(a, b, c, '调用了add')
})

























