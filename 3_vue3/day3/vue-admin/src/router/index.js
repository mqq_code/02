import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/home/Home.vue'
import Dashboard from '../views/home/dashboard/Dashboard.vue'
// import Notfound from '../views/404/404.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
      redirect: '/dashboard',
      meta: {
        isLogin: true,
        abc: 'abcdefg',
        title: '首页'
      },
      children: [
        { path: '/dashboard', name: 'dashboard', meta: { title: '监控页' }, component: Dashboard },
        { path: '/list', name: 'list', meta: { title: '列表页面' }, component: () => import('../views/home/list/List.vue') },
        { path: '/user', name: 'user', meta: { title: '个人中心' }, component: () => import('../views/home/user/User.vue') }
      ]
    },
    {
      path: '/login',
      name: 'login',
      meta: { title: '登录' },
      component: () => import('../views/login/Login.vue')
    },
    // {
    //   name: '404',
    //   component: Notfound
    // }
  ]
})


router.beforeEach((to) => {
  document.title = to.meta.title
  if (to.meta.isLogin) {
    const token = localStorage.getItem('token')
    if (!token) {
      return {
        path: '/login',
        query: {
          redirect: encodeURIComponent(to.fullPath)
        }
      }
    }
  }
})

export default router
