import { defineStore } from 'pinia'
import { ref } from 'vue'
import { userInfoApi } from '@/services'

export const useUserStore = defineStore('user', () => {
  const userInfo = ref({})

  const setUser = async () => {
    const res = await userInfoApi()

    if (res.data.code === 0) {
      userInfo.value = res.data.values
    }
  }

  return {
    userInfo,
    setUser
  }
})