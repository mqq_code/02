import axios from 'axios'
import { ElMessage } from 'element-plus'
import router from '../router'

axios.defaults.baseURL = 'http://121.89.213.194:9001'

// 请求拦截器
// 添加请求拦截器
axios.interceptors.request.use(function (config) {
  console.log('=========== 请求拦截器', config)
  // 使用 axios 调用接口时，发送请求之前会先执行此函数，可以给所有接口传入公共参数
  config.headers.Authorization = localStorage.getItem('token')
  config.headers.abc = 'ABCDEFG'
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

// 响应拦截器
axios.interceptors.response.use(function (response) {
  // 2xx 范围内的状态码都会触发该函数。
  // 对响应数据做点什么
  return response;
}, function (error) {
  // 超出 2xx 范围的状态码都会触发该函数。
  // 对响应错误做点什么
  if (error.response.status === 401) {
    ElMessage.error('登录信息失效，请重新登录！')
    localStorage.removeItem('token')
    router.push('/login')
  }
  return Promise.reject(error);
})


// 统一管理所有接口
export const loginApi = (params) => {
  return axios.post('/api/login', params)
}

export const registerApi = (params) => {
  return axios.post('/api/register', params)
}

export const userInfoApi = () => {
  return axios.get('/api/user/info')
}

export const userListApi = (params) => {
  return axios.get('/api/userlist', {
    params
  })
}

export const delApi = (id) => {
  return axios.post('/api/user/delete', { id })
}

export const createApi = (params) => {
  return axios.post('/api/user/create', params)
}

export const updateApi = (params) => {
  return axios.post('/api/user/update', params)
}

export const exportApi = (ids) => {
  return axios.post('/api/export', { ids }, { responseType: 'blob' })
}



// 200
// 301 永久重定向
// 302 临时重定向
// 304 协商缓存
// 401 登录失效
// 403 没有权限
// 404 资源不存在
// 500 服务器内部错误
// 502 网关错误