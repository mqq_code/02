import { ref, onBeforeUnmount, watch } from 'vue'

export const useCount = (n) => {
  const count = ref(n)
  let timer = null

  watch(count, () => {
    if (count.value === 0) {
      stop()
    }
  })

  const start = () => {
    timer = setInterval(() => {
      count.value--
    }, 1000)
  }

  const stop = () => {
    clearInterval(timer)
  }

  onBeforeUnmount(() => {
    stop()
  })

  return {
    count,
    start,
    stop
  }
}