import { ref, onMounted, onUpdated, onBeforeUnmount } from 'vue'

// 封装公共逻辑，名字以use开头
export const usePos = (elRef) => {

  console.log(elRef)

  const pos = ref({
    x: 0,
    y: 0
  })
  const isDown = ref(false)

  const mousedown = () => {
    isDown.value = true
  }
  const mousemove = e => {
    if (isDown.value) {
      pos.value.x = e.pageX
      pos.value.y = e.pageY
    }
  }
  const mouseup = () => {
    isDown.value = false
  }
  onMounted(() => {
    const el = elRef?.value || document
    el.addEventListener('mousedown', mousedown)
    el.addEventListener('mousemove', mousemove)
    el.addEventListener('mouseup', mouseup)
  })
  onBeforeUnmount(() => {
    const el = elRef?.value || document
    el.removeEventListener('mousedown', mousedown)
    el.removeEventListener('mousemove', mousemove)
    el.removeEventListener('mouseup', mouseup)
  })

  onUpdated(() => {
    console.log('组件更新了')
  })


  return pos
}