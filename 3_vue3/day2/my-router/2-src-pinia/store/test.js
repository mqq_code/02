import { defineStore } from 'pinia'
import { ref, computed } from 'vue'
import { useUserStore } from './user'

// 创建store
export const useTestStore = defineStore('test123', () => {
  // 调用其他store
  const userStore = useUserStore()

  const banner = ref([])

  console.log(userStore.username)


  const getBanner = () => {
    fetch('https://zyxcl.xyz/music_api/banner')
      .then(res => res.json())
      .then(res => {
        banner.value = res.banners
      })
  }

  return {
    banner,
    getBanner
  }
})