import { defineStore } from 'pinia'
import { ref, computed } from 'vue'

// 创建store
export const useUserStore = defineStore('user', () => {

  const username = ref('王小明')
  const age = ref(22)

  const len = computed(() => {
    return username.value.toString().length + age.value
  })

  const changeName = name => {
    username.value = name
  }

  return {
    username,
    age,
    len,
    changeName
  }
})