"use strict";
{
    const s = 100;
    function fn(a) {
        if (typeof a === 'string') {
            // 添加类型保护，在此 if 中确定 a 是字符串类型
            console.log(a.split(''));
        }
        else {
            a.push(100);
        }
    }
    fn([100]);
    const abc = {
        name: 'aa',
        age: 20
    };
    const sex22 = '女';
}
