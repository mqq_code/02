"use strict";
{
    // 泛型: 类型的形参，当函数中的某个或者某些变量类型只有调用时才能确定类型时可以使用泛型
    // function fn<T>(a: T): T {
    //   return a
    // }
    // let a = fn<string>('aaa')
    // function createArr<T>(n: number, a: T): T[] {
    //   const arr = new Array(n).fill(a)
    //   return arr
    // }
    // const arr1 = createArr<number>(10, 10)
    // const arr2 = createArr<string>(5, 'a')
    // const arr3 = createArr<{ name: string; }>(5, { name: '小明' })
    // console.log(arr3)
    // function ref<T>(params: T): { value: T } {
    //   return {
    //     value: params
    //   }
    // }
    const ref = (params) => {
        return { value: params };
    };
    const num = ref(0);
    const arr5 = ref([]);
    arr5.value.push(100);
    const obj = {
        name: 'aaa',
        sex: 19,
        hobby: 'aaaa'
    };
    const cat = {
        name: 'xxxxx',
        age: 10,
        sex: 1
    };
    // const arr: deepArr<number>[] = [[[[[1,[2,3],4],5],6],7], 8]
    // function flat<T>(arr: deepArr<T>[]): T[] {
    //   return arr.reduce((prev: T[], val) => {
    //     return prev.concat(Array.isArray(val) ? flat(val) : val)
    //   }, [] as T[])
    // }
    // const a1 = flat([['a',[['b'],'c'],'d'],'e'])
}
