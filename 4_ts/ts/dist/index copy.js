"use strict";
{
    // typeof: 返回一个js变量的类型
    const obj = {
        name: '小明',
        age: 100
    };
    const arr = [1, 2, 3, 4, 5];
    const sum = (a, b) => a + b;
    const k = 'hobby';
    const xm = {
        name: 'xmm',
        age: 22,
        sex: '男'
    };
    let Sex;
    (function (Sex) {
        Sex[Sex["women"] = 1] = "women";
        Sex[Sex["men"] = 2] = "men";
        Sex[Sex["other"] = 3] = "other";
    })(Sex || (Sex = {}));
    const sex = 'men';
    // console.log(Sex[sex])
    // 泛型约束： U 必须符合 extends 后的条件
    function getVal(obj, key) {
        return obj[key];
    }
    const xhh = {
        aa: '小红',
        bb: 22,
        cc: '女'
    };
    console.log(getVal(xhh, 'bb'));
    function getLen(params) {
        return params.length;
    }
    console.log(getLen([1, 2, 3, 4, 5]));
    console.log(getLen('abcdefg'));
    console.log(getLen({ a: 100, length: 10 }));
    const abc = 'aaaa';
    const props = {
        title: 'aaa',
        sex: 1
    };
    const props2 = {
        title: 'aaa',
        sex: '男'
    };
}
