"use strict";
{
    const obj = {
        name: 'xm',
        say(text) {
        },
        say1(text) {
        }
    };
    obj.abc = 100;
    const tom = {
        name: '汤姆',
        color: 'blue'
    };
    const xmm = {
        name: 'xmm',
        color: '黄色',
        sex: '公猫',
        food: ['猫粮']
    };
    // type fn = (t: number) => number;
    const f = (t) => {
        return t;
    };
    // type xm = {
    //   name: string;
    //   age: number;
    // }
    // type xm = {
    //   sex: number;
    // }
    const xm1 = {
        name: 'xxx',
        age: 10,
        sex: 10
    };
    window.abc = 100;
    console.log(window.location);
    // interface 和 type：
    // 1. interface 定义类型，type 是定义类型别名
    // 2. type 可以定义基础类型、联合类型、交叉类型， interface 不行
    // 3. interface 重名时会合并，可以扩展现有的 interface 定义的类型，例如扩展 window 对象
    // 4. type 重名会报错
    // 5. interface 使用 extends 继承，type 使用交叉类型扩展
}
