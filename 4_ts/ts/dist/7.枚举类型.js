"use strict";
{
    // 枚举: 存数字或字母的映射关系，可以使用方便记忆的单词描述一组数字或者字母
    let Sex;
    (function (Sex) {
        Sex[Sex["men"] = 1] = "men";
        Sex[Sex["women"] = 2] = "women";
        Sex[Sex["other"] = 3] = "other";
    })(Sex || (Sex = {}));
    let SortType;
    (function (SortType) {
        SortType[SortType["default"] = 0] = "default";
        SortType[SortType["up"] = 1] = "up";
        SortType[SortType["down"] = 2] = "down";
    })(SortType || (SortType = {}));
    let Direction;
    (function (Direction) {
        Direction[Direction["Up"] = 87] = "Up";
        Direction[Direction["Down"] = 83] = "Down";
        Direction[Direction["Left"] = 65] = "Left";
        Direction[Direction["Right"] = 68] = "Right";
    })(Direction || (Direction = {}));
    const box = document.querySelector('.box');
    document.addEventListener('keydown', e => {
        if (e.keyCode === Direction.Left) {
            box.style.left = box.offsetLeft - 10 + 'px';
        }
        else if (e.keyCode === Direction.Up) {
            box.style.top = box.offsetTop - 10 + 'px';
        }
        else if (e.keyCode === Direction.Right) {
            box.style.left = box.offsetLeft + 10 + 'px';
        }
        else if (e.keyCode === Direction.Down) {
            box.style.top = box.offsetTop + 10 + 'px';
        }
    });
}
