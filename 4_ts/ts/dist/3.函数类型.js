"use strict";
{
    // 函数
    function fn1(a, b) {
        return a + b;
    }
    const fn2 = function (a, b) {
        console.log(a, b);
    };
    const fn3 = (a, b) => {
        return b.name;
    };
    const fn4 = (a) => a + 100;
    const obj = {
        say(a, b) {
            return b;
        }
    };
    // 函数可选参数
    function fn5(a, b = 100, c) {
        console.log(a, b);
    }
    function fn6({ name, age } = { name: 'aa', age: 18 }) {
        console.log(name, age);
    }
    fn6({
        name: 'xm',
        age: 22
    });
    // 剩余参数
    function fn7(a, ...params) {
        console.log(params);
    }
    function fn8(a, b) {
        if (b) {
            return b;
        }
        else {
            return a;
        }
    }
    let aa = fn8(100, 'aa');
    const xm = {
        name: 'xm',
        say(text) {
            return text.length;
        }
    };
}
