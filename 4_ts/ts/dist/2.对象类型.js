"use strict";
var _a;
{
    // 对象类型
    const xm1 = {
        name: '小明',
        age: 20,
        sex: '男'
    };
    const xm2 = {
        name: '小明',
        age: 20,
        sex: '女'
    };
    const xm3 = {
        name: '小明33333',
        age: 20
    };
    // 可选链
    (_a = xm3.sex) === null || _a === void 0 ? void 0 : _a.slice(1);
    const list = [
        {
            title: '许嵩',
            songs: [
                { name: '有何不可', time: 150 },
                { name: '有何不可111', time: 1500 }
            ]
        },
        {
            title: '许嵩111',
            songs: [
                { name: '有何不可1111', time: 150 },
                { name: '有何不可1112222', time: 1500 }
            ]
        }
    ];
    const tree = [
        {
            title: '标题1',
            count: 0,
            children: [
                { title: '标题11', count: 1 },
                {
                    title: '标题12',
                    count: 1,
                    children: [
                        { title: '标题121', count: 2 },
                    ]
                }
            ]
        },
        {
            title: '标题2',
            count: 2
        },
    ];
}
