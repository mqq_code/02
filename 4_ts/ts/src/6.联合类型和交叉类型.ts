{

  // 联合类型: 只能访问公共的属性和方法
  type sex = number | string | string[]
  const s: sex = 100

  function fn(a: number[] | string) {
    if (typeof a === 'string') {
      // 添加类型保护，在此 if 中确定 a 是字符串类型
      console.log(a.split(''))
    } else {
      a.push(100)
    }
  }
  fn([100])

  // 交叉类型: 会把多个类型合并，不能合并不兼容的类型
  type aa = { name: string; } & { age: number; }
  const abc: aa = {
    name: 'aa',
    age: 20
  }


  // 文字类型
  type sex1 = '男' | '女' | '其他'
  const sex22: sex1 = '女'

}