{

  interface person {
    name: string;
    age?: number;
    sex: '男' | '女';
    hobby: string[];
  }


  // 内置泛型
  type readonlyPerson = Readonly<person>
  type partialPerson = Partial<person>
  type RequiredPerson = Required<person>
  // 挑选指定属性组成新的对象类型
  type pickPerson = Pick<person, 'name' | 'age'>
  // 排除指定属性组成新的对象类型
  type omitPerson = Omit<person, 'name' | 'age'>

  type A = string | null | undefined | boolean
  type B = number | string

  // 返回两个类型公有的子类型
  type ExtractAB = Extract<A, B>
  // 从 A 中排出 B 中存在的类型
  type ExcludeAB = Exclude<A, B>

  // 返回一个对象类型，key是a、b、c、d， value的类型是第二个参数
  type obj = Record<'a' | 'b' | 'c' | 'd', number | string>

  const sum = (a: number, b: number) => [a, b]
  // 获取函数的返回值类型
  type Ires = ReturnType<typeof sum>







  const h1 = document.querySelector('h1')
  // 排除参数中的 null 和 undefined
  type a = NonNullable<typeof h1>







  // 0. typescript 有什么优势
  // 1. any、void、never、unknown
  // 2. type 和 interface
  // 3. 什么是泛型
  // 4. 常用的内置泛型








}