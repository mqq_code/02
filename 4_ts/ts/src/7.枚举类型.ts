{

  // 枚举: 存数字或字母的映射关系，可以使用方便记忆的单词描述一组数字或者字母
  enum Sex {
    men = 1,
    women = 2,
    other = 3
  }

  enum SortType {
    default = 0,
    up = 1,
    down = 2
  }


  enum Direction {
    Up = 87,
    Down = 83,
    Left = 65,
    Right = 68
  }

  const box = document.querySelector('.box') as HTMLDivElement
  document.addEventListener('keydown', e => {
    if (e.keyCode === Direction.Left) {
      box.style.left = box.offsetLeft - 10 + 'px'
    } else if (e.keyCode === Direction.Up) {
      box.style.top = box.offsetTop - 10 + 'px'
    } else if (e.keyCode === Direction.Right) {
      box.style.left = box.offsetLeft + 10 + 'px'
    } else if (e.keyCode === Direction.Down) {
      box.style.top = box.offsetTop + 10 + 'px'
    }
  })






}