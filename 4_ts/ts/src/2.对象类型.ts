{

  // 对象类型
  const xm1: {
    name: string;
    age: number;
    sex: '男' | '女'
  } = {
    name: '小明',
    age: 20,
    sex: '男'
  }
  
  // 类型别名
  type person = {
    name: string;
    readonly age: number;
    sex?: '男' | '女' // 可选属性
  }
  
  const xm2: person  = {
    name: '小明',
    age: 20,
    sex: '女'
  }
  const xm3: person  = {
    name: '小明33333',
    age: 20
  }
  // 可选链
  xm3.sex?.slice(1)
  
  
  
  // 对象数组
  type song = {
    name: string;
    time: number
  }
  type listItem = {
    title: string;
    songs: song[]
  }
  const list: listItem[] = [
    {
      title: '许嵩',
      songs: [
        { name: '有何不可', time: 150 },
        { name: '有何不可111', time: 1500 }
      ]
    },
    {
      title: '许嵩111',
      songs: [
        { name: '有何不可1111', time: 150 },
        { name: '有何不可1112222', time: 1500 }
      ]
    }
  ]
  
  type treeItem = {
    title: string;
    count: number;
    children?: treeItem[]
  }
  const tree: treeItem[] = [
    {
      title: '标题1',
      count: 0,
      children: [
        { title: '标题11', count: 1 },
        {
          title: '标题12',
          count: 1,
          children: [
            { title: '标题121', count: 2 },
          ]
        }
      ]
    },
    {
      title: '标题2',
      count: 2
    },
  ]
  
  
  
  
  
  }