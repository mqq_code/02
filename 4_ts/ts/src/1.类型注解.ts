{
// 类型：string、number、boolean、null、undefined、symbol、array、object、function
// any、void、never、unknown
// js本身时弱类型语言，ts增加了静态类型，编译时是强类型

// 类型注解
let num: number = 100
let str: string = '100'
let show: boolean = true
let nu: null = null
let un: undefined = undefined
let s: symbol = Symbol('aaaa')
let s1: symbol = Symbol('aaaa')

// symbol使用场景：可以给对象定义属性，此属性不会被新增的属性覆盖
const obj = {
  [s]: 100,
  [s1]: 'aaaaaaaa'
}
console.log(obj[s])

// 定义数组
// 方式1:
const arr: number[] = [1,2,3,4,5]
// 方式2: 泛型方式
const arr1: Array<string> = ['a', 'b', 'c']

// 元组：定义已知类型和长度的数组
const arr2: [string, number, boolean] = ['a', 100, true]

// any: 任意类型，相当于放弃类型校验
let a1: any = 100
a1 = ''
a1 = true
let arr3: any[] = [1, 'a', true]
arr3.push()


// void: 没有值，函数没有返回值时使用
function log(): void {
  console.log(123)
}
let aaa = log()

// never: 永远不会存在的值
let abc: string & number

// unknown: 暂时不确定变量类型，又不希望放弃校验，可以使用类型断言确定变量类型
let unk: unknown
unk = 100

if (typeof unk === 'number') {
  unk.toFixed()
}
// 类型断言 确定变量类型
(unk as number).toFixed()





}