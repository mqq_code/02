

{



  // 类型断言和类型推论

  // 类型推论：ts会根据变量的值反推变量的类型
  let num = 100
  let arr = [1,2,3,4,5,6]
  let arr2 = arr.map(v => {
    return {
      title: Math.random(),
      val: v
    }
  })

  const box = document.querySelector('div')
  const p = document.querySelector('p')
  const button = document.querySelector('button')
  // const inp = document.querySelector('input')!


  // 类型断言: 当开发者比编译器更确定变量的类型时使用

  // ! 非空断言
  const h1 = document.querySelector('h1')
  h1!.addEventListener('click', e => {
    console.log(e.target)
  })


  // 类型断言
  const inp = document.querySelector('.inp') as HTMLInputElement
  inp.addEventListener('change', e => {
    console.log((e.target as HTMLInputElement).value)
    // console.log((<HTMLInputElement>e.target).value)
  })

}