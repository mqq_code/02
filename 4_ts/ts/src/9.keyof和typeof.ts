{

  // typeof: 返回一个js变量的类型
  const obj = {
    name: '小明',
    age: 100
  }
  const arr = [1,2,3,4,5]
  const sum = (a: number, b: number) => a + b

  type Iobj = typeof obj
  type Iarr = typeof arr
  type Isum = typeof sum


  // keyof: 返回对象类型所有的key组成的联合类型
  type person = {
    name: string;
    age: number;
    sex: 0 | 1;
    hobby: string[];
  }

  type keys = keyof person // 'name' | 'age' | 'sex' | 'hobby‘

  const k: keys = 'hobby'



  const xm = {
    name: 'xmm',
    age: 22,
    sex: '男'
  }
  type xmKey = keyof typeof xm


  enum Sex {
    women = 1,
    men = 2,
    other = 3
  }
  type keySex = keyof typeof Sex
  const sex: keySex = 'men'
  // console.log(Sex[sex])





// 泛型约束： U 必须符合 extends 后的条件
function getVal<T, U extends keyof T>(obj: T, key: U) {
  return obj[key]
}


const xhh = {
  aa: '小红',
  bb: 22,
  cc: '女'
}

console.log(getVal(xhh, 'bb')) 



function getLen<T extends { length: number }>(params: T): number {
  return params.length
}

console.log(getLen([1,2,3,4,5]))
console.log(getLen('abcdefg'))
console.log(getLen({ a: 100, length: 10 }))


// 条件类型
type Iabc<T> = T extends { length: number } ? string : boolean

const abc: Iabc<[]> = 'aaaa'


// 泛型默认值
interface IProps<T = 0 | 1> {
  title: string;
  sex: T
}


const props: IProps = {
  title: 'aaa',
  sex: 1
}

const props2: IProps<'男' | '女'> = {
  title: 'aaa',
  sex: '男'
}

}