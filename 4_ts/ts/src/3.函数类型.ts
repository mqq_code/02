

{

  // 函数
  function fn1(a: number, b: string): string {
    return a + b
  }
  
  const fn2 = function (a: string, b: number[]): void {
    console.log(a, b)
  }
  
  const fn3 = (a: boolean, b: { name: string }): string => {
    return b.name
  }
  
  const fn4 = (a: number): number => a + 100 
  
  const obj = {
    say(a: number, b: string): string {
      return b
    }
  }
  
  
  // 函数可选参数
  function fn5(a: string, b: number = 100, c?: boolean) {
    console.log(a, b)
  }
  // fn5('a')


  // 参数解构赋值
  type person = {
    name: string;
    age: number;
  }
  function fn6({ name, age }: person = { name: 'aa', age: 18 }) {
    console.log(name, age)
  }
  fn6({
    name: 'xm',
    age: 22
  })


  // 剩余参数
  function fn7(a: string, ...params: number[]) {
    console.log(params)
  }
  // fn7('aaaa', 1,2,3,4,5,6,7)




// 函数重载
function fn8(a: string): number
function fn8(a: number, b: string): string
function fn8(a: any, b?: any): any {
  if (b) {
    return b
  } else {
    return a
  }
}
let aa = fn8(100, 'aa')



// 对象中的函数类型
type obj1 = {
  name: string;
  // say: (text: string) => number;
  say(text: string): number;
}
const xm: obj1 = {
  name: 'xm',
  say(text: string): number {
    return text.length
  }
}








}