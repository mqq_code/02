{
  // 接口： 定义对象类型
  interface person {
    readonly name: string;
    age?: number;
    say: (text: string) => void;
    say1(text: string): void;
    // 允许对象新增其他类型的参数
    [k: string]: any;
  }

  const obj: person = {
    name: 'xm',
    say(text: string) { 
    },
    say1(text: string) {
    }
  }

  obj.abc = 100


  // 接口继承
  interface animal {
    name: string;
    color: string;
  }

  const tom: animal = {
    name: '汤姆',
    color: 'blue'
  }

  // 继承现有的类型
  // interface cat extends animal {
  //   sex: string;
  //   food: string[]
  // }

  // type 使用交叉类型扩展:
  type cat = animal & {
    sex: string;
    food: string[]
  }
  const xmm: cat = {
    name: 'xmm',
    color: '黄色',
    sex: '公猫',
    food: ['猫粮']
  }






  // 接口定义函数类型
  interface fn {
    (t: number): number;
  }
  // type fn = (t: number) => number;
  const f: fn = (t) => {
    return t
  }

  // 区别：
  // interface  重名时会合并，可以扩展现有的 interface 定义的类型，例如扩展 window 对象
  // type 重名会报错
  // interface 使用 extends 继承，type 使用交叉类型扩展

  interface xm {
    name: string;
    age: number;
  }
  interface xm {
    sex: number;
  }

  // type xm = {
  //   name: string;
  //   age: number;
  // }
  // type xm = {
  //   sex: number;
  // }

  const xm1: xm = {
    name: 'xxx',
    age: 10,
    sex: 10
  }

  window.abc = 100

  console.log(window.location)







  // interface 和 type：
  // 1. interface 定义类型，type 是定义类型别名
  // 2. type 可以定义基础类型、联合类型、交叉类型， interface 不行
  // 3. interface 重名时会合并，可以扩展现有的 interface 定义的类型，例如扩展 window 对象
  // 4. type 重名会报错
  // 5. interface 使用 extends 继承，type 使用交叉类型扩展

}