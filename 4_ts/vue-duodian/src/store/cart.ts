import { defineStore } from 'pinia'
import { ref, computed } from 'vue'
import type { GoodsItem } from '@/services'

type CartItem = GoodsItem & { checked: boolean }

export const useCartStore = defineStore('cart', () => {
  const cartList = ref<CartItem[]>([])

  const total = computed(() => {
    return cartList.value.reduce((prev, val) => {
      return {
        count: prev.count + (val.checked ? val.count : 0),
        price: prev.price + (val.checked ? val.count * val.price : 0),
      }
    }, { count: 0, price: 0 })
  })

  const push = (goods: GoodsItem) => {
    const index = cartList.value.findIndex(v => v.id === goods.id)
    if (index > -1) {
      cartList.value[index].count++
    } else {
      cartList.value.push({...goods, count: 1, checked: true})
    }
  }
  const changeCount = (id: string, num: number) => {
    const index = cartList.value.findIndex(v => v.id === id)
    cartList.value[index].count += num
    if (cartList.value[index].count === 0) {
      cartList.value.splice(index, 1)
    }
  }
  return {
    cartList,
    total,
    push,
    changeCount
  }
})