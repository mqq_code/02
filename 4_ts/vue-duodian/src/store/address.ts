import { defineStore } from 'pinia'
import { ref, computed } from 'vue'

export interface Address {
  id: string;
  name: string;
  tel: string;
  address: string;
  isDefault?: boolean;
  province?: string;
  city?: string;
  county?: string;
}
// type Address = Record<'id' | 'name' | 'tel' | 'address', string>
type Info = Omit<Address, 'id'>

export const useAddressStore = defineStore('address', () => {
  const chosenAddressId = ref('')
  const addressList = ref<Address[]>([
    {
      address: "adsfas",
      city: "吉林市",
      county: "吉林高新技术产业开发区",
      id: "1720602994633",
      isDefault: false,
      name: "aaa",
      province: "吉林省",
      tel: "15122223333",
    }
  ])

  const currentAddress = computed(() => {
    return addressList.value.find(v => v.id === chosenAddressId.value)
  })
  
  const addAddress = (info: Info) => {
    if (info.isDefault) {
      addressList.value.forEach(item => {
        item.isDefault = false
      })
    }
    addressList.value.push({
      id: Date.now() + '',
      ...info
    })
  }

  return {
    chosenAddressId,
    addressList,
    currentAddress,
    addAddress
  }
})