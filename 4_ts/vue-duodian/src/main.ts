import './style/common.scss'
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import {
  Button,
  Form,
  Field,
  Toast,
  Tabbar,
  TabbarItem,
  Watermark,
  Tab,
  Tabs,
  Sticky,
  Cell,
  CellGroup,
  Swipe,
  SwipeItem,
  AddressList,
  AddressEdit,
  Dialog,
  GridItem,
  Grid,
  ActionBar,
  ActionBarIcon,
  ActionBarButton,
  Card,
  ContactCard,
  SubmitBar,
  Notify,
  Empty,
  Checkbox,
  CheckboxGroup,
  Uploader
} from 'vant'
import 'vant/lib/index.css'
import './mock'
import App from './App.vue'
import router from './router'
import VConsole from 'vconsole'


console.log('环境变量', process.env.NODE_ENV)
console.log('vite环境变量', import.meta.env)

// 判断运行环境
if (process.env.NODE_ENV === 'development') {
  const vConsole = new VConsole()
}




const app = createApp(App)


app.use(ActionBar)
app.use(ActionBarIcon)
app.use(ActionBarButton)
app.use(Button)
app.use(Form)
app.use(Field)
app.use(Toast)
app.use(Tabbar)
app.use(TabbarItem)
app.use(Watermark)
app.use(Tab)
app.use(Tabs)
app.use(Sticky)
app.use(Cell)
app.use(CellGroup)
app.use(Swipe)
app.use(SwipeItem)
app.use(AddressList)
app.use(AddressEdit)
app.use(Dialog)
app.use(Grid)
app.use(GridItem)
app.use(Card)
app.use(ContactCard)
app.use(SubmitBar)
app.use(Notify)
app.use(Empty)
app.use(Checkbox)
app.use(CheckboxGroup)
app.use(Uploader)

app.use(createPinia())
app.use(router)

app.mount('#app')
