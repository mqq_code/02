import axios from 'axios'

// axios.defaults.baseURL = 'https://zyxcl.xyz'

export interface BannerItem {
  targetId: number;
  imageUrl: string;
}
export interface bannerRes {
  code: number;
  banners: BannerItem[]
}

let host = 'https://zyxcl.xyz'

if (process.env.NODE_ENV === 'production') {
  host = '线上域名'
}



export const getBannerApi = () => {
  return axios.get<bannerRes>(`${host}/music_api/banner`)
}

export const getSuggestionApi = ({ query, region }: { query: string, region: string }) => {
  return axios.get('/map/place/v2/suggestion', {
    params: {
      ak: 'nqdUuCXZg7BNjuKReZHTsWbc0L28hYrF',
      query,
      region,
      city_limit: true,
      output: 'json'
    }
  })
}


export interface GoodsItem {
  id: string;
  name: string;
  price: number;
  count: number;
  desc: string;
  images: string[];
  cover: string;
}
export interface GoodsRes {
  code: number;
  msg: string;
  list: GoodsItem[];
}

export const getGoodsApi = () => {
  return axios.get<GoodsRes>('/api/goods')
}

export interface GoodsDetailRes {
  code: number;
  msg: string;
  info: GoodsItem;
}
export const getGoodsDetailApi = (id: string) => {
  return axios.post<GoodsDetailRes>('/api/goods/detail', { id })
}

