import Mock from 'mockjs'

const data = Mock.mock({
  'list|20': [{
    'id': '@id',
    'name': '@cname',
    'price|1-100': 100,
    'count': 0,
    'desc': '@cparagraph(50, 150)',
    'images|5': ['@image(375x150, @color)'],
    'cover': '@image(100x100, @color)'
  }]
})

// 模拟接口
Mock.mock('/api/login', 'post', (options) => {
  const { username, password } = JSON.parse(options.body)
  if (username === 'zyx' && password === '123') {
    return {
      code: 0,
      msg: '登录成功',
      token: username + password
    }
  }
  return {
    code: -1,
    msg: '用户名或密码错误'
  }
})

Mock.mock('/api/goods', 'get', (options) => {
  return {
    code: 0,
    msg: '成功',
    list: data.list
  }
})

Mock.mock('/api/goods/detail', 'post', (options) => {
  const { id } = JSON.parse(options.body)
  const goods = data.list.find((v: any) => v.id === id)
  if (goods) {
    return {
      code: 0,
      msg: '成功',
      info: goods
    }
  }
  return {
    code: -1,
    msg: '商品不存在'
  }
})