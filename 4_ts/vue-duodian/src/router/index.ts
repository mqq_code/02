import { createRouter, createWebHistory } from 'vue-router'
import Home from '@/views/home/Home.vue'
import Cart from '@/views/home/cart/Cart.vue'
import Mall from '@/views/home/mall/Mall.vue'
import Mine from '@/views/home/mine/Mine.vue'
import Login from '@/views/login/Login.vue'
import AddressList from '@/views/addressList/AddressList.vue'
import EditAddress from '@/views/editAddress/EditAddress.vue'
import Detail from '@/views/detail/Detail.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      meta: { isLogin: true },
      component: Home,
      redirect: '/mall',
      children: [
        { path: '/mall', name: 'mall', component: Mall },
        { path: '/cart', name: 'cart', component: Cart },
        { path: '/mine', name: 'mine', component: Mine }
      ]
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/detail',
      name: 'detail',
      meta: { isLogin: true },
      component: Detail
    },
    {
      path: '/address/list',
      name: 'addressList',
      meta: { isLogin: true },
      component: AddressList
    },
    {
      path: '/address/edit',
      name: 'editAddress',
      meta: { isLogin: true },
      component: EditAddress
    }
  ]
})

router.beforeEach((to, from) => {
  if (to.meta.isLogin) {
    const token = localStorage.getItem('token')
    if (!token) {
      return {
        path: '/login',
        query: {
          redirect: encodeURIComponent(to.fullPath)
        }
      }
    }
  }
})

export default router
