// ts中有两种文件
// .ts  可以写类型和逻辑
// .d.ts  类型声明文件，只能定义类型

// 声明全局变量的类型
declare let aaaa: number;
declare const VERSION: string;
declare function getVersion(): string;
declare interface Window {
  $abc: string;
}

// declare module 'mockjs' {
//   declare let a: string;
//   declare function fn(): string;
// }