import { createApp } from 'vue'
import { createPinia } from 'pinia'
import Mock from 'mockjs'
// 如果使用的插件没有类型声明文件尝试从 @types/包名 下载声明文件
// npm i --save-dev @types/包名

import axios from 'axios'


import App from './App.vue'
import router from './router'

const app = createApp(App)

app.use(createPinia())
app.use(router)

app.mount('#app')
